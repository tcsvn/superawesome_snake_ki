package Util.awesome.graph;
import Logic.Field;
import Logic.Field.CellType;
import Logic.Point;
import java.io.Serializable;
import java.util.LinkedList;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.BellmanFordShortestPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;



/**
 * The GameGraph is a Graph where every Vertex represents a Point with a Celltype
 * of the Gamefield. A Graph is generated for each step a snake makes. This 
 * alternate representation of the Gamefield makes it easier to apply algorithms
 * use differing representation of the gamefield (see Graphcontainer).
 * It is also used to calculate steps between the snakes head
 * and features. Or maximal avaliable steps for a snake.
 */

public class GameGraph 
        extends SimpleDirectedWeightedGraph<GameVertex, DefaultWeightedEdge>
        implements Serializable {    
    
/*----------------------------------------------------------------------------*/
//  CONSTRUCTORS
/*----------------------------------------------------------------------------*/         
    public GameGraph(Field field) {
        super(DefaultWeightedEdge.class);
        generateGraphAll(field);
    }  
    
/*----------------------------------------------------------------------------*/
//  Generation of Graph
/*----------------------------------------------------------------------------*/  
    /**
     * create a Graph that contains all Points of the Field
     * @param field
     * @return 
     */
    public void generateGraphAll(Field field){
        for (int x = 0; x < field.width(); x++) {
            for (int y = 0; y < field.height(); y++) {
                GameVertex tempGameVertex = new GameVertex(
                            new Point(x, y),
                            field.cell(new Point(x,y)));
                addVertex(tempGameVertex);                
                // check if there is a vertex left to connect to
                if (containsVertexWithPoint(new Point(x-1,y))) {
                    GameVertex leftNeighbour = getVertexWithPoint(new Point(x-1,y));
                    addEdge(tempGameVertex, leftNeighbour);
                    addEdge(leftNeighbour, tempGameVertex);
                }
                // check if there is a vertex above to connect to 
                if (containsVertexWithPoint(new Point(x, y - 1))) {
                    GameVertex topNeighbour = getVertexWithPoint(new Point(x,y-1));
                    addEdge(tempGameVertex, topNeighbour);
                    addEdge(topNeighbour, tempGameVertex);
                }
            }
        }
        
        // if thorus is active the borders are connected accordingly
        if(field.getFieldIsOpen()){
            // connects from top to bottom
            for(int y = 1; y < field.height()/2; y++){                
                Point right = new Point(field.width()-1, field.height()-y);
                Point left = new Point(0, y);
                if(containsVertexWithPoint(left) && containsVertexWithPoint(right)){
                    connectVertex(
                            getVertexWithPoint(right),
                            getVertexWithPoint(left));
                }
            }
            //connects from left to right
            for(int x = 1; x < field.width()/2; x++){
                Point up = new Point(x, 0);
                Point bottom = new Point(field.width()-x, field.height()-1);
                if (containsVertexWithPoint(up) && containsVertexWithPoint(bottom)){
                    connectVertex(
                            getVertexWithPoint(bottom), 
                            getVertexWithPoint(up));
                }
            }
        }
    }
    
    
/*----------------------------------------------------------------------------*/
//  Simple Graph Operations
/*----------------------------------------------------------------------------*/   
    
    /**
     * return true if the Graph contains a vertex with the given Point
     * @param point
     * @return 
     */
    public boolean containsVertexWithPoint(Point point){
        for(GameVertex ver : this.vertexSet()){            
            if(ver.equalsInPoint(new GameVertex(point, CellType.WALL))){
                return true;
            }
        }
        return false;
    }
    
    /**
     * return true if a vertice exists in the graph, that is of the Celltype
     * @param ct
     * @return 
     */
    public boolean containsVertexWithCellType(CellType ct){
        for(GameVertex vert : this.vertexSet()){
            if(vert.equalsInCellType(new GameVertex(new Point(0,0), ct))){
                return true;
            }
        }
        return false;
    }
    
    /**
     * returns the vertex with the given Point if the graph contains the vertex
     * if the graph doesn't contain it "null" is returned instead
     * @param point
     * @return 
     */
    public GameVertex getVertexWithPoint(Point point){
        for(GameVertex ver : this.vertexSet()){            
            if(ver.equalsInPoint(new GameVertex(point, CellType.WALL))){
                return ver;
            }
        }
        return null;
    }
    
    /**
     * Get all vertices of a given CellType
     * @param ct
     * @return 
     */
    public LinkedList<GameVertex> getVerticesWithCelltype(CellType ct){
        LinkedList <GameVertex> verticeList = new LinkedList<>();
        for(GameVertex ver : this.vertexSet()){
            if(ver.equalsInCellType(new GameVertex(
                    new Point(0,0),
                    ct))){
                verticeList.add(ver);
            }
        }
        return verticeList;
    }
    
    /**
     * change the Celltype of a vertice of a given Point to a given CellType.
     * If the Vertice doesn't exist already a new one is generated and added
     * @param graph
     * @param point
     * @param ct 
     */
    public void changePointToCellType(
            Point point,
            CellType ct){
        if(containsVertexWithPoint(point)){
            GameVertex tmpVertice = getVertexWithPoint(point);
            tmpVertice.cellType = ct;
        }
        else{
            addPointToGraph(point, ct);
        }
    }
 
    /**
     * adds a Point to a Graph
     * @param graph
     * @param point
     * @param ct
     * @return 
     */
    public void addPointToGraph(
            Point point, 
            CellType ct){
        GameVertex newVertex = new GameVertex(point, ct);
        // add head of snake to graph
        addVertex(newVertex);
        GameVertex topNeighbour = getVertexWithPoint(new Point(point.x, point.y - 1));
        GameVertex bottomNeighbour = getVertexWithPoint(new Point(point.x, point.y + 1));
        GameVertex leftNeighbour = getVertexWithPoint(new Point(point.x - 1, point.y));
        GameVertex rightNeighbour = getVertexWithPoint(new Point(point.x + 1, point.y));
        
        //connect vertice head to graph
        connectVertex(newVertex, topNeighbour);
        connectVertex(newVertex, bottomNeighbour);        
        connectVertex(newVertex, leftNeighbour);
        connectVertex(newVertex, rightNeighbour);
    }
    
    /**
     * removes all Vertex from the Graph that have the given CellType
     * @param type 
     */
    public void removeVertexOfType(CellType type){        
        LinkedList<GameVertex> toDeleteList = new LinkedList<>();
        for(GameVertex gv : vertexSet()){
            if(gv.cellType == type){
                toDeleteList.add(gv);          
            }
        }
        for(GameVertex gv : toDeleteList){
            this.removeVertex(gv);
        }
    }
    
    /**
     * connects to Vertices with each other bidirectional
     * @param graph
     * @param source
     * @param sink
     * @return 
     */
    public void connectVertex(GameVertex source, GameVertex sink) {
        if (containsVertex(sink)) {
            addEdge(source, sink);
            addEdge(sink, source);
        }        
    }
    
    
/*----------------------------------------------------------------------------*/
//  Algorithms for Graph
/*----------------------------------------------------------------------------*/     
    
    /**
     * return the shortest amount of Steps needed to get from one Point to another
     * @param point1
     * @param point2
     * @param gameInfo
     * @return 
     */
    public int calcShortestStepsTo(GameVertex v1,GameVertex v2){        
        DijkstraShortestPath<GameVertex, DefaultWeightedEdge> djikstraAlgo = 
                new DijkstraShortestPath<>(this, Double.POSITIVE_INFINITY);
        return (int) djikstraAlgo.getPathWeight(v1, v2);
        
    }
    
    /**
     * calculates the steps from a given point to the next appearance of a 
     * celltype
     * @param point
     * @param ct
     * @return 
     */
    public int calcStepsToNearestCellType(Point point, CellType ct){
        int min = Integer.MAX_VALUE;
        try {
            // get "target" vertices
            LinkedList <GameVertex> targetVertices;
            targetVertices = this.getVerticesWithCelltype(ct);            
            // get "from" vertice
            GameVertex from = this.getVertexWithPoint(point);
            for(GameVertex target : targetVertices){
                int stepsToCell = this.calcShortestStepsTo(
                        from, 
                        target);
                if(stepsToCell < min){
                    min = stepsToCell;
                }
            }
            return min;
        } // is thrown if eithes Head or celltype is not part of the graph 
        catch (IllegalArgumentException e) {
            return -1;
        }    
    }
    
    /**
     * calculates the maximal Steps that are possible from a given Point 
     * @param from
     * @param tempGraph
     * @return 
     */
    public int calcMaxStepsPossibleFrom(Point from, GameGraph tempGraph){
//        GameGraph tempGraph = SerializationUtils.clone(this);       
        // Set all edge wheights to negative. then use bellman ford to get the 
        // minimal path which is (because of the negative wheights) the 
        // maximum path    
        for(DefaultWeightedEdge edge : tempGraph.edgeSet()){
            tempGraph.setEdgeWeight(edge, -1);
        }
        // get "from" vertex
        GameVertex from1 = tempGraph.getVertexWithPoint(from); // doesn't return vertex
        
        // use bellman ford to get the maximum path to each vertex
        BellmanFordShortestPath<GameVertex, DefaultWeightedEdge> bf
                = new BellmanFordShortestPath<>(tempGraph);        
        ShortestPathAlgorithm.SingleSourcePaths<GameVertex, DefaultWeightedEdge> paths 
                = bf.getPaths(from1);
        
        // get maximal steps for for all paths
        int max =0;                      
        for(GameVertex sink : vertexSet()){
            double val;
            val = Math.abs(paths.getWeight(sink));
            if(val >  max){
                max = (int) val;
            }
        }       
        return max;
    }    
}
