package Util.awesome.graph;
import Logic.Point;
import Logic.Snake;
import java.util.LinkedList;
import javafx.scene.paint.Color;

/**
 * This is just used to make a copy of a Snake, to simulate moves without moving
 * the real snake. 
 */
public class SnakeExt extends Snake{
    private LinkedList<Point> segments;
    
    /**
     * constructor that assigns the given snakes segments to himself
     * @param segments 
     */
    public SnakeExt(LinkedList<Point> segments){
        super(null, null, null, Color.CORAL);
        this.segments = createSegemnts(segments);
    }
    
    /**
     * gets segments and returns a new Linked list with the same points
     * @param segments
     * @return 
     */
    public LinkedList<Point> createSegemnts(LinkedList<Point> segments){
        LinkedList<Point> result = new LinkedList<Point>();
        for(Point point : segments){
            result.add(new Point(point.x, point.y));
        }
        return result;
    }
    
/*----------------------------------------------------------------------------*/
//  Getter and Setter
/*----------------------------------------------------------------------------*/    
    
    public Point headPosition() {
	return segments.getLast();
    }
    
    public LinkedList<Point> segments() {
        return segments;
    }
    
    public void setHead(Point portal) {
        segments.addLast(portal);	
    }
    
    /**
     * simulates one move in the direction the given new Head was moved
     * @param newHead 
     */
    public void simulateMove(Point newHead){
        segments.addLast(newHead);
        segments.removeFirst();
    }

}
