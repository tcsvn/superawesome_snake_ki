package Util.awesome.graph;
import Logic.Field.CellType;
import Logic.Point;
import java.io.Serializable;

/**
 * A GameVertex represents a Point of the Gamefield containing the Point (x,y)
 * and the CellType
 */
public class GameVertex implements Serializable{
    Point point;
    CellType cellType;
    
/*----------------------------------------------------------------------------*/
//  CONSTRUCTORS
/*----------------------------------------------------------------------------*/     
    public GameVertex(Point point, CellType cellType){
        this.point = point;
        this.cellType=cellType;
    }
    
    /**
     * If this instance of a Vertex has the same Point as the given. True is 
     * returned
     * @param obj
     * @return 
     */
    public boolean equalsInPoint(Object obj){
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        GameVertex other = (GameVertex) obj;
        return point.x == other.point.x && point.y == other.point.y;
    }
    
    /**
     * if this Instance of a Vertex has the same Celltype as the Given 
     * true is returned
     * @param obj
     * @return 
     */
    public boolean equalsInCellType(Object obj){
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        GameVertex other = (GameVertex) obj;
        return cellType == other.cellType;
    }    
    
    /**
     * if this instance of a vertex has the same Celltype and the same Point
     * true is returned
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
            if (this == obj)
                    return true;
            if (obj == null)
                    return false;
            if (getClass() != obj.getClass())
                    return false;
            GameVertex other = (GameVertex) obj;
            return point.x == other.point.x
                    && point.y == other.point.y
                    && cellType == other.cellType;                    
    }  
}
