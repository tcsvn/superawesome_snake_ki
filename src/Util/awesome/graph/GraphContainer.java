package Util.awesome.graph;
import Logic.Field;
import Logic.Field.CellType;
import Logic.Point;
import Logic.Snake;
import java.io.Serializable;
import java.util.LinkedList;
import org.apache.commons.lang3.SerializationUtils;

/**
 * This class is for managing a different Graphs. As it is for example necessary
 * when calculating the steps to an apple to exclude all other features different
 * graphs are generated for each use case. This class manages the access and 
 * generation off the graphs. 
 * First a represenation of the whole gamefield (graphAll) then by deleting
 * nodes all the others are made.
 * @author Chris
 */
public class GraphContainer implements Serializable{
    
    GameGraph graphAll;
    GameGraph graphAllFeatures;
    GameGraph graphOnlySpace;    
    GameGraph graphOnlyApple;
    GameGraph graphOnlyPortals;
    GameGraph graphOnlyFeatureWall;
    GameGraph graphOnlyFeatureChangeHeadTail;
    GameGraph graphOnlyFeatureChangeSnake;
    GameGraph graphOnlyFeatureCutTail;
    GameGraph graphOnlyFeatureOpenField;
    GameGraph graphOnlyFeatureSpeedUp;
    
    
    /**
     * enum representating each graphtype
     */
    public enum GraphType {
        ALL,
        ALL_FEATURES,
        ONLY_APPLE, 
        ONLY_PORTALS,
        ONLY_FEATURE_WALL,
        ONLY_FEATURE_CHANGEHEADTAIL,
        ONLY_FEATURE_CHANGESNAKE,
        ONLY_FEATURE_SPEEDUP,
        ONLY_FEATURE_CUTTAIL,
        ONLY_FEATURE_OPENFIELD,
        ONLY_SPACE
    }
/*----------------------------------------------------------------------------*/
//  CONSTRUCTORS
/*----------------------------------------------------------------------------*/     
    
    public GraphContainer(Point snakeHead, Field field){        
        graphAll = new GameGraph(field);     
        graphAll.removeVertexOfType(CellType.WALL);
        
        // create the other graphs that contain only their feature
        graphOnlyApple = generateGraphOnlyApple(graphAll);
        graphOnlyPortals = generateGraphOnlyFeaturePortal(graphAll);
        graphOnlyFeatureWall = generateGraphOnlyFeatureWall(graphAll);
        graphOnlyFeatureChangeHeadTail = generateGraphOnlyFeatureChangeHeadTail(graphAll);
        graphOnlyFeatureChangeSnake = generateGraphOnlyFeatureChangeSnake(graphAll);
        graphOnlyFeatureSpeedUp = generateGraphOnlyFeatureSpeedUp(graphAll);
        graphOnlyFeatureCutTail = generateGraphOnlyFeatureCutTail(graphAll);
        graphOnlyFeatureOpenField = generateGraphOnlyFeatureOpenMap(graphAll);
    }
    
/*----------------------------------------------------------------------------*/
//  Generation of other graphs
/*----------------------------------------------------------------------------*/         
    /**
     * generates a graph, that only consists out of spaces and apples
     * @param graph
     * @return 
     */
    private GameGraph generateGraphOnlyApple(GameGraph graph){
        GameGraph result = SerializationUtils.clone(graph);        
        result.removeVertexOfType(CellType.CHANGEHEADTAIL);
        result.removeVertexOfType(CellType.CHANGESNAKE);
        result.removeVertexOfType(CellType.FEATUREWALL);
        result.removeVertexOfType(CellType.PORTAL);
        result.removeVertexOfType(CellType.SNAKE);
        result.removeVertexOfType(CellType.SPEEDUP);
        result.removeVertexOfType(CellType.CUTTAIL);
        result.removeVertexOfType(CellType.OPENFIELD);
        return result;
    }    
    
    /**
     * generates a graph, that only consists out of spaces and the portals
     * @param graph
     * @return 
     */        
    private GameGraph generateGraphOnlyFeaturePortal(GameGraph graph){
        GameGraph result = SerializationUtils.clone(graph);               
        result.removeVertexOfType(CellType.APPLE);
        result.removeVertexOfType(CellType.CHANGEHEADTAIL);
        result.removeVertexOfType(CellType.CHANGESNAKE);
        result.removeVertexOfType(CellType.FEATUREWALL);        
        result.removeVertexOfType(CellType.SNAKE);        
        result.removeVertexOfType(CellType.SPEEDUP);
        result.removeVertexOfType(CellType.CUTTAIL);
        result.removeVertexOfType(CellType.OPENFIELD);        
        return result;
    }
    
    /**
     * generates a graph, that only consists out of spaces and the feature
     * wall
     * @param graph
     * @return 
     */    
    private GameGraph generateGraphOnlyFeatureWall(GameGraph graph){
        GameGraph result = SerializationUtils.clone(graph);        
        result.removeVertexOfType(CellType.APPLE);
        result.removeVertexOfType(CellType.CHANGEHEADTAIL);
        result.removeVertexOfType(CellType.CHANGESNAKE);
        result.removeVertexOfType(CellType.PORTAL);        
        result.removeVertexOfType(CellType.SNAKE);
        result.removeVertexOfType(CellType.SPEEDUP);
        result.removeVertexOfType(CellType.CUTTAIL);
        result.removeVertexOfType(CellType.OPENFIELD);        
        return result;
    }

    /**
     * generates a graph, that only consists out of spaces and the feature
     * head tail
     * @param graph
     * @return 
     */        
    private GameGraph generateGraphOnlyFeatureChangeHeadTail(GameGraph graph){
        GameGraph result = SerializationUtils.clone(graph);        
        result.removeVertexOfType(CellType.APPLE);
        result.removeVertexOfType(CellType.PORTAL);
        result.removeVertexOfType(CellType.CHANGESNAKE);
        result.removeVertexOfType(CellType.FEATUREWALL);        
        result.removeVertexOfType(CellType.SNAKE);
        result.removeVertexOfType(CellType.SPEEDUP);
        result.removeVertexOfType(CellType.CUTTAIL);
        result.removeVertexOfType(CellType.OPENFIELD);        
        return result;
    }

    /**
     * generates a graph, that only consists out of spaces and the feature
     * ChangeSnake
     * @param graph
     * @return 
     */        
    private GameGraph generateGraphOnlyFeatureChangeSnake(GameGraph graph){
        GameGraph result = SerializationUtils.clone(graph);        
        result.removeVertexOfType(CellType.APPLE);
        result.removeVertexOfType(CellType.CHANGEHEADTAIL);
        result.removeVertexOfType(CellType.PORTAL);
        result.removeVertexOfType(CellType.FEATUREWALL);        
        result.removeVertexOfType(CellType.SNAKE);
        result.removeVertexOfType(CellType.SPEEDUP);
        result.removeVertexOfType(CellType.CUTTAIL);
        result.removeVertexOfType(CellType.OPENFIELD);        
        return result;
    }

    /**
     * generates a graph, that only consists out of spaces and the feature
     * SpeedUp
     * @param graph
     * @return 
     */        
    private GameGraph generateGraphOnlyFeatureSpeedUp(GameGraph graph){
        GameGraph result = SerializationUtils.clone(graph);        
        result.removeVertexOfType(CellType.APPLE);
        result.removeVertexOfType(CellType.CHANGEHEADTAIL);
        result.removeVertexOfType(CellType.PORTAL);
        result.removeVertexOfType(CellType.FEATUREWALL);        
        result.removeVertexOfType(CellType.SNAKE);
        result.removeVertexOfType(CellType.CHANGESNAKE);
        result.removeVertexOfType(CellType.CUTTAIL);
        result.removeVertexOfType(CellType.OPENFIELD);
        return result;
    }

    /**
     * generates a graph, that only consists out of spaces and the feature
     * CutTail
     * @param graph
     * @return 
     */        
    private GameGraph generateGraphOnlyFeatureCutTail(GameGraph graph){
        GameGraph result = SerializationUtils.clone(graph);        
        result.removeVertexOfType(CellType.APPLE);
        result.removeVertexOfType(CellType.CHANGEHEADTAIL);
        result.removeVertexOfType(CellType.PORTAL);
        result.removeVertexOfType(CellType.FEATUREWALL);        
        result.removeVertexOfType(CellType.SNAKE);
        result.removeVertexOfType(CellType.SPEEDUP);        
        result.removeVertexOfType(CellType.CHANGESNAKE);
        result.removeVertexOfType(CellType.OPENFIELD);        
        return result;
    }

    /**
     * generates a graph, that only consists out of spaces and the feature
     * OpenMap
     * @param graph
     * @return 
     */        
    private GameGraph generateGraphOnlyFeatureOpenMap(GameGraph graph){
        GameGraph result = SerializationUtils.clone(graph);        
        result.removeVertexOfType(CellType.APPLE);
        result.removeVertexOfType(CellType.CHANGEHEADTAIL);
        result.removeVertexOfType(CellType.PORTAL);
        result.removeVertexOfType(CellType.FEATUREWALL);        
        result.removeVertexOfType(CellType.SNAKE);
        result.removeVertexOfType(CellType.SPEEDUP);        
        result.removeVertexOfType(CellType.CHANGESNAKE);
        result.removeVertexOfType(CellType.CUTTAIL);        
        return result;
    }    
    
//------------------------------------------------------------------------------
    /**
     * simulates movement of a snake by adding a head to each graph and setting
     * the tail to a space
     * @param newHead
     * @param snake 
     */
    public void simulateSnakeMovement(Point newHead, Snake snake){
        // add head to Graph
        for(GraphType gt: GraphType.values()){
            switch (gt){
                case ALL:{
                    graphAll.addPointToGraph(
                            newHead, 
                            CellType.SNAKE);
                    graphAll.changePointToCellType(
                            snake.segments().getFirst(), 
                            CellType.SPACE);
                    break;
                }
                case ONLY_APPLE: {
                    graphOnlyApple.addPointToGraph(
                            newHead, 
                            CellType.SNAKE);
                    graphOnlyApple.changePointToCellType(
                            snake.segments().getFirst(), 
                            CellType.SPACE);                    
                    break;
                }                    
                case ONLY_PORTALS: {
                    graphOnlyPortals.addPointToGraph(
                            newHead, 
                            CellType.SNAKE);
                    graphOnlyPortals.changePointToCellType(
                            snake.segments().getFirst(), 
                            CellType.SPACE);                    
                    break;                    
                }
                case ONLY_FEATURE_CHANGEHEADTAIL: {
                    graphOnlyFeatureChangeHeadTail.addPointToGraph(
                            newHead, 
                            CellType.SNAKE);
                    graphOnlyFeatureChangeHeadTail.changePointToCellType(
                            snake.segments().getFirst(), 
                            CellType.SPACE);                    
                    break;                    
                }
                case ONLY_FEATURE_CHANGESNAKE: {
                    graphOnlyFeatureChangeSnake.addPointToGraph(
                            newHead, 
                            CellType.SNAKE);
                    graphOnlyFeatureChangeSnake.changePointToCellType(
                            snake.segments().getFirst(), 
                            CellType.SPACE);                    
                    break;                    
                }                
                case ONLY_FEATURE_WALL: {
                    graphOnlyFeatureWall.addPointToGraph(
                            newHead, 
                            CellType.SNAKE);
                    graphOnlyFeatureWall.changePointToCellType(
                            snake.segments().getFirst(), 
                            CellType.SPACE);                    
                    break;                    
                }       
                case ONLY_FEATURE_CUTTAIL:{
                    graphOnlyFeatureCutTail.addPointToGraph(
                            newHead, 
                            CellType.SNAKE);
                    graphOnlyFeatureCutTail.addPointToGraph(
                            newHead, 
                            CellType.SPACE);
                    break;   
                }
                case ONLY_FEATURE_OPENFIELD:{
                    graphOnlyFeatureOpenField.addPointToGraph(
                            newHead, 
                            CellType.SNAKE);
                    graphOnlyFeatureOpenField.addPointToGraph(
                            newHead, 
                            CellType.SPACE);
                    break;   
                }
                case ONLY_FEATURE_SPEEDUP:{
                    graphOnlyFeatureSpeedUp.addPointToGraph(
                            newHead, 
                            CellType.SNAKE);
                    graphOnlyFeatureSpeedUp.addPointToGraph(
                            newHead, 
                            CellType.SPACE);
                    break;   
                }
            }
        }
    }

//------------------------------------------------------------------------------
    /**
     * returns the graph specified by a given Graphtype
     * @param gt
     * @return 
     */
    public GameGraph selectGraph(GraphType gt){
        switch (gt){
            case ALL:
                return graphAll;
            case ONLY_APPLE:
                return graphOnlyApple;
            case ONLY_PORTALS:
                return graphOnlyPortals;
            case ONLY_FEATURE_CHANGEHEADTAIL:
                return graphOnlyFeatureChangeHeadTail;
            case ONLY_FEATURE_CHANGESNAKE:
                return graphOnlyFeatureChangeSnake;
            case ONLY_FEATURE_WALL:
                return graphOnlyFeatureWall;                
            case ALL_FEATURES:
                return graphAllFeatures;
            case ONLY_FEATURE_CUTTAIL:
                return graphOnlyFeatureCutTail;
            case ONLY_FEATURE_OPENFIELD:
                return graphOnlyFeatureOpenField;
            case ONLY_FEATURE_SPEEDUP:
                return graphOnlyFeatureSpeedUp;
            case ONLY_SPACE:
                return graphOnlySpace;
            default:
                return  graphAll;
            }
    }

//------------------------------------------------------------------------------
    /**
     * returns a copy of the Graph specified by given GraphType
     * @param gt
     * @return 
     */
    public GameGraph cloneGraphOfType(GraphType gt){
        switch(gt){
                case ALL: 
                    return SerializationUtils.clone(this.graphAll);
                case ONLY_APPLE:
                    return SerializationUtils.clone(this.graphOnlyApple);
                case ONLY_PORTALS:
                    return SerializationUtils.clone(this.graphOnlyPortals);
                case ONLY_FEATURE_CHANGEHEADTAIL:
                    return SerializationUtils.clone(this.graphOnlyFeatureChangeHeadTail);
                case ONLY_FEATURE_CHANGESNAKE:
                    return SerializationUtils.clone(this.graphOnlyFeatureChangeSnake);
                case ONLY_FEATURE_WALL:
                    return SerializationUtils.clone(this.graphOnlyFeatureWall);                    
                case ALL_FEATURES:
                    return SerializationUtils.clone(this.graphAllFeatures);
                case ONLY_FEATURE_CUTTAIL:
                    return SerializationUtils.clone(this.graphOnlyFeatureCutTail);
                case ONLY_FEATURE_OPENFIELD:
                    return SerializationUtils.clone(this.graphOnlyFeatureOpenField);
                case ONLY_FEATURE_SPEEDUP:
                    return SerializationUtils.clone(this.graphOnlyFeatureSpeedUp);
                case ONLY_SPACE:
                    return SerializationUtils.clone(this.graphOnlySpace);
                default: 
                    return null;
        }        
    }
    
    
//-----------------------ALGORITHMS---------------------------------------------    
    
    /**
     * he function returns the shortest path to the nearest Celltype
     * from a given point in a given graph
     * @param ct
     * @return 
     */
    public int calcStepsToNearestCellType(Point point, CellType ct, GraphType gt){
        GameGraph tempGraph = selectGraph(gt);
        return tempGraph.calcStepsToNearestCellType(point, ct);
    }
    
    /**
     * the function returns the maximal steps that can be made from a given
     * point in a given graph
     * @param from
     * @param gt
     * @return 
     */
    public int calcMaxStepsPossibleFrom(Point from, GraphType gt){ 
        GameGraph tempGameGraph =SerializationUtils.clone(selectGraph(gt));        
        // add snake head to graph
        tempGameGraph.addPointToGraph(from, CellType.SNAKE);
        int result = tempGameGraph.calcMaxStepsPossibleFrom(from, tempGameGraph);        
        return result;
    }
         
    
    /**
     * the function returns the Point where an Apple is or null if there is no 
     * Apple
     * @return 
     */
    public Point getApplePos(){
        GameGraph tempGraph = selectGraph(GraphType.ALL);
        if(tempGraph.containsVertexWithCellType(CellType.APPLE)){
            LinkedList<GameVertex> st = 
                selectGraph(GraphContainer.GraphType.ALL).getVerticesWithCelltype(CellType.APPLE);
            for(GameVertex ver :st){
                return ver.point;
            }
            // if no apple is found
            return new Point(0,0);
        }
        else{
            return null;
        }
    }
}
