package Util.awesome.threading;

import java.util.concurrent.TimeUnit;

import Brains.AwesomeBrain;
import Util.awesome.graph.GraphContainer;
import Util.awesome.threading.FunctionThread;
import Logic.*;
import Logic.Snake.Direction;

/**
 * Handles an thread to calculate the score for a given direction.
 * Creates sub threads for every scoring function. Then waits until all sub threads finished
 * their calculations and creates a scoring array out of the values.
 * 
 * @author Flo
 *
 */
public class DirectionThread extends Thread {

	private Snake snake;
	private GameInfo gameInfo;
	private GraphContainer graphInfo;
	AwesomeBrain a;// = new AwesomeNormalBrain();
	
	// the count of functios where an input for a direction is derived
    private final int amountOfFunctionsUsedForInput=12;
    //score array for calculation
	private double[] directionValues = new double[amountOfFunctionsUsedForInput];
	// array where all function threads are stored
	FunctionThread functionThreads[] = new FunctionThread[amountOfFunctionsUsedForInput];

	public enum ScoringFunction {
		STEPSTOAPPLE, DISTTOHEADENEMY, DISTTOOWNTAIL, SPEED, MAXSTEPSENEMY, MAXSTEPSOWN, CHANGESNAKE, WALL, CHANGEHEADTAIL, PORTAL, CUTTAIL, OPENFIELD
	}

	public DirectionThread(Snake snake, GraphContainer graphInfo, GameInfo gameInfo, AwesomeBrain a) {

		this.snake = snake;
		this.gameInfo = gameInfo;
		this.a = a;
		this.graphInfo = graphInfo;
	}

	public double[] getDirectionValues() {
		return directionValues;
	}

	public void run() {

		// long startTime = System.currentTimeMillis();
		// System.out.println(d + " gestartet");

		// creates Threads for every scoring function
		
		functionThreads[0] = new FunctionThread(ScoringFunction.STEPSTOAPPLE, snake, graphInfo, gameInfo, a);
		//functionThreads[1] = new FunctionThread(ScoringFunction.DISTTOHEADENEMY, snake, graphInfo, gameInfo, a);
		//functionThreads[2] = new FunctionThread(ScoringFunction.DISTTOOWNTAIL, snake, graphInfo, gameInfo, a);
		functionThreads[3] = new FunctionThread(ScoringFunction.MAXSTEPSENEMY, snake, graphInfo, gameInfo, a);
		functionThreads[4] = new FunctionThread(ScoringFunction.MAXSTEPSOWN, snake, graphInfo, gameInfo, a);
		functionThreads[5] = new FunctionThread(ScoringFunction.CHANGESNAKE, snake, graphInfo, gameInfo, a);
		functionThreads[6] = new FunctionThread(ScoringFunction.WALL, snake, graphInfo, gameInfo, a);
		functionThreads[7] = new FunctionThread(ScoringFunction.CUTTAIL, snake, graphInfo, gameInfo, a);
		functionThreads[8] = new FunctionThread(ScoringFunction.OPENFIELD, snake, graphInfo, gameInfo, a);
		functionThreads[9] = new FunctionThread(ScoringFunction.CHANGEHEADTAIL, snake, graphInfo, gameInfo, a);
		functionThreads[10] = new FunctionThread(ScoringFunction.PORTAL, snake, graphInfo, gameInfo, a);
		functionThreads[11] = new FunctionThread(ScoringFunction.SPEED, snake, graphInfo, gameInfo, a);

		

		// starts all threads
		functionThreads[0].start();
		for (int i = 3; i < amountOfFunctionsUsedForInput; i++) {
			functionThreads[i].start();
		}

		// waits for every thread until it finished
		try {
			functionThreads[0].join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 3; i < amountOfFunctionsUsedForInput; i++) {
			try {
				functionThreads[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			;
		}

		// fills the function array with the calculated values
		directionValues[0] = functionThreads[0].getScoreFunctionValue();
		for (int i = 3; i < amountOfFunctionsUsedForInput; i++) {
			directionValues[i] = functionThreads[i].getScoreFunctionValue();

		}
		/*
		 * // System.out.println(d + " fertig. Wert: " + directionValues[0]); //
		 * startTime = System.currentTimeMillis() - startTime;
		 * 
		 * // System.out.println(d + " beendet Dauer: " + startTime);
		 * 
		 */

	}

}
