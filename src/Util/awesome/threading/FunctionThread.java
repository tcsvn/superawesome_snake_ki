package Util.awesome.threading;

import Util.awesome.graph.GraphContainer;
import Util.awesome.threading.DirectionThread.ScoringFunction;
import Brains.AwesomeBrain;
import Logic.*;
import Logic.Snake.Direction;

/**
 * Handles a thread that calculates the outcome for a given scoring function.
 * Starts the scoring function given in the constructor and calculates its score
 * 
 * @author Flo
 *
 */
public class FunctionThread extends Thread {

	
	private ScoringFunction function;
	private Snake snake;
	private GameInfo gameInfo;
	private GraphContainer graphInfo;
	AwesomeBrain a; 
	private double scoreFunctionValue;

	public double getScoreFunctionValue() {
		return scoreFunctionValue;
	}

	FunctionThread(ScoringFunction function,  Snake snake, GraphContainer graphInfo, GameInfo gameInfo, AwesomeBrain a) {
		this.function = function;
		
		this.snake = snake;
		this.gameInfo = gameInfo;
		this.a = a;
		this.graphInfo= graphInfo;
	}

	/**
	 * overrites run() from class Thread
	 * decides which scoring function should be used in the thread
	 */
	public void run() {

		// System.out.println(function + "start");

		
		switch (function) {
		case STEPSTOAPPLE:
			scoreFunctionValue = a.calcStepsToApple(snake,graphInfo, gameInfo);
			break;

		case DISTTOHEADENEMY:
			scoreFunctionValue = a.distanceToHeadEnemy(snake, gameInfo);
			break;

		case DISTTOOWNTAIL:
			scoreFunctionValue = a.distanceToOwnTail(snake);
			break;

		case SPEED:
			scoreFunctionValue = a.calcStepsToFeature_SpeedUP(snake,graphInfo, gameInfo);
			break;

		case MAXSTEPSENEMY:
			scoreFunctionValue = a.maxStepsEnemy(snake,graphInfo, gameInfo);
			break;
			
		case MAXSTEPSOWN:
			scoreFunctionValue = a.maxStepsOwnSnake(snake, graphInfo, gameInfo);
			break;

		case CHANGESNAKE:
			scoreFunctionValue = a.calcStepsToFeature_ChangeSnake(snake,graphInfo, gameInfo);
			break;

		case WALL:
			scoreFunctionValue = a.calcStepsToFeature_Wall(snake,graphInfo, gameInfo);
			break;

		case CHANGEHEADTAIL:
			scoreFunctionValue = a.calcStepsToFeature_ChangeHeadTail(snake,graphInfo, gameInfo);
			break;

		case PORTAL:
			scoreFunctionValue = a.calcStepsToFeature_Portal(snake,graphInfo, gameInfo);
			break;
			
		case CUTTAIL:
			scoreFunctionValue = a.calcStepsToFeature_CutTail(snake,graphInfo, gameInfo);
			break;
			
		case OPENFIELD:
			scoreFunctionValue = a.calcStepsToFeature_OpenField(snake,graphInfo, gameInfo);
			break;

		}
		
		//System.out.println(function + ": " + scoreFunctionValue);
	}

}
