package Util.awesome.threading;
import Brains.AwesomeBrain;
import Logic.SnakeBrain;
import java.util.LinkedList;

/**
 * For each individual in a population of the Evolution Chamber (util.awesome.simulation)
 * This Thread is created. it starts the RunnableGame threads
 */
public class GA_IndividualThread implements Runnable {
    int id;
    int gamesPerCombination;
    AwesomeBrain brain;
    LinkedList<SnakeBrain> enemyBrains;

    public GA_IndividualThread(
            int id,
            int gamesPerCombination,
            AwesomeBrain brain,
            LinkedList<SnakeBrain> enemyBrains) {
        this.id = id;
        this.gamesPerCombination = gamesPerCombination;
        this.brain = brain;
        this.enemyBrains = enemyBrains;
    }

    /**
     * is called when Thread is created
     */
    @Override
    public void run() {
//            System.out.println( id + " started PopulationThread ");
        double fitness = RunnableGame.runThreads(
                id,
                this.gamesPerCombination,
                brain,
                enemyBrains);
        brain.setFitness(fitness);
//            System.out.println( id + " finished PopulationThread ");
    }
}
