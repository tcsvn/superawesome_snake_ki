package Util.awesome.threading;
import Brains.AwesomeBrain;
import Brains.AwesomeNormalBrain;
import Brains.RandomBrain;
import Logic.Apple;
import Logic.Field;
import Logic.Game;
import Logic.Point;
import Logic.Snake;
import Logic.SnakeBrain;
import java.util.ArrayList;
import java.util.LinkedList;
import javafx.scene.paint.Color;
import org.apache.commons.lang3.SerializationUtils;

/**
 * The runnable Game simulates a game against an awesomeBrain with given Enemys
 * Each RunnableGame contains the score the AwesomeBrain has made and whether 
 * it had won. in runThreads an amount of RunnableGames are spawned and the
 * mean value of the score (given awesome had won) is returned.
 */
public class RunnableGame extends Thread {
    private double score;
    private boolean awesomeHasWon;
    private int id;    
    private AwesomeBrain awBrain;
    private SnakeBrain enemyBrain;


/*----------------------------------------------------------------------------*/
//  CONSTRUCTORS
/*----------------------------------------------------------------------------*/
    RunnableGame(int id, AwesomeBrain awBrain, SnakeBrain enemyBrain) {
        this.id = id;
        this.awBrain = awBrain;
        this.enemyBrain = enemyBrain;
    }

/*----------------------------------------------------------------------------*/
//  INIT Methods
/*----------------------------------------------------------------------------*/
    /**
     * these methods are just for initalizing the game like in Logic.game
     * 
     */
    private Field initField() {
        Field field = Field.defaultField(30, 20);
        field.addApple(new Apple(50, 1, new Point(1,2)), new Point(1,2));
        return field;
    }

    private ArrayList<Color> initColors() {
        ArrayList<Color> colors = new ArrayList<Color>();
        colors.add(Color.VIOLET);
        colors.add(Color.AZURE);
        return colors;
    }

    private ArrayList<Point> initStartPositions() {
        ArrayList<Point> startPositions = new ArrayList<Point>();
        startPositions.add(new Point(2, 2));
        startPositions.add(new Point(27, 17));
        return startPositions;
    }

    private ArrayList<SnakeBrain> initSnakeBrains() {
        ArrayList<SnakeBrain> brains = new ArrayList<SnakeBrain>();
        // add here snake 
        brains.add(awBrain);
        brains.add(enemyBrain);
        return brains;
    }
    
    private double[] initProbabilitys() {
        return new double[] {1, 0.005, 0.002, 0.002, 0.005, 0.001, 0.002};
    }
    
/*----------------------------------------------------------------------------*/
//  Getter and Setter
/*----------------------------------------------------------------------------*/    
    
   public double getGameScore(){
        return this.score;
    }
    public boolean getAwesomeHasWon(){
        return this.awesomeHasWon;
    }

    /**
     * returns the AwesomeNormalBrain or AwesomeNeuralBrain of a given Snake A
     * arraylist
     * @param snakes
     * @return 
     */
    private AwesomeBrain getChildOfAwesomeBrain(ArrayList<Snake> snakes) {
        AwesomeBrain awesome = null;
        for (Snake snk : snakes) {
            // nifty hack because isInstance, is not working :/
            String s1 = "" + snk.getBrain().getClass().getSuperclass();
            if (s1.contains("AwesomeBrain")) {
                awesome = (AwesomeBrain) snk.getBrain();
                return awesome;
            }
        }
        return null;
    }
    
    /**
     * returns the snake of an arraylist if it contains an AwesomeBrain
     * @param snakes
     * @return 
     */
    private Snake getSnakeOfAwesomeBrain(ArrayList<Snake> snakes) {
        for (Snake snk : snakes) {
            // nifty hack because isInstance, is not working :/
            String s1 = "" + snk.getBrain().getClass().getSuperclass();
            if (s1.contains("AwesomeBrain")) {                
                return snk;
            }
        }
        return null;
    }    
    
        
/*----------------------------------------------------------------------------*/
//  LOGIC
/*----------------------------------------------------------------------------*/
    /**
     * is called from thread. runs a single game and sets the values if awesome
     * snake has won and its score to the RunnableGame instanz
     */
    public void run() {
            Game game = new Game(
                    this.initSnakeBrains(),
                    this.initStartPositions(),
                    this.initColors(),
                    this.initField(),
                    this.initProbabilitys());
            game.setOutput(false);
            game.run();            
            /*
            instead of game.run()
            for (int s =0; s < 1000; s++){
                game.nextStep();
            }
            */
            // Get Score of Snake
            AwesomeBrain awesome = getChildOfAwesomeBrain(game.getSnakes());
            this.score = getSnakeOfAwesomeBrain(game.getSnakes()).getScore();
            
            // Check if the Snake has won
            Snake awesomeSnake = getSnakeOfAwesomeBrain(game.getSnakes());
            this.awesomeHasWon =awesomeSnake.alive();                       
    }

    /**
     * Returns the mean fitness AwesomeBrain has made against alot of enemy
     * if a snake wins a game, the snakescore is included in the calculation of the 
     * meanscore of all games. The meanscore is then returned and used as fitness
     * for the AwesomeBrains
     *
     * @param applePropability
     * @param gamesAgainstSingleEnemy
     * @param awBrain
     * @param enemySnake
     * @return
     */
    public static double runThreads(
            int id,
            int gamesAgainstSingleEnemy,
            AwesomeBrain awBrain,
            LinkedList<SnakeBrain> enemyBrains) {
        
        int totalAmountGames = gamesAgainstSingleEnemy*enemyBrains.size();
        
        // clone awesomeBrain into gamesPerCombination independent Objects                              
        RunnableGame[] threadArray = new RunnableGame[totalAmountGames];
        for (int i = 0; i < totalAmountGames; i++) {
            AwesomeBrain temp  = SerializationUtils.clone(awBrain);
            // reset Fitness of clone
            temp.setFitness(0.0);
            SnakeBrain enemyBrain = enemyBrains.get((i%enemyBrains.size()));
            threadArray[i] = new RunnableGame(i*10+id,  temp, enemyBrain);
        }
        
        // run all Threads
        for (int i = 0; i < totalAmountGames; i++) {
            threadArray[i].start();            
        }
        
        // check if there is at least one thread still alive
        boolean allAlive = true;
        while (allAlive) {
            allAlive = false;
            for (int i = 0; i < totalAmountGames; i++) {
                if (threadArray[i].isAlive()) {
                    allAlive = true;
                }
            }
        }
        
        //calculate Fitness
        double meanFitness =0;
        for(int i=0; i < totalAmountGames ; i++){
            if(threadArray[i].getAwesomeHasWon()){
                meanFitness += threadArray[i].getGameScore();
            }
            else{
                meanFitness += threadArray[i].getGameScore()*0.75;
            }
        }
        meanFitness = meanFitness * (1/(double)(totalAmountGames));
        return meanFitness;
    }
}
