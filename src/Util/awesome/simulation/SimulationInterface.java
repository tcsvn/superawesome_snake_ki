package Util.awesome.simulation;

/**
 * This interface has to be Implemented by Every Simulation like 
 *  BruteForce 
 *  EvolutionChamber
 * @author Chris
 */
public interface SimulationInterface {
    public void printSimulationInformation();
    public void runSimulation();    
    public void printSolution();
    public void saveSolution();
}
