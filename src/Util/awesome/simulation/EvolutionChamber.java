package Util.awesome.simulation;
import Util.awesome.simulation.SimulationInterface;
import Util.awesome.simulation.BruteForce;
import Brains.AwesomeBrain;
import Brains.AwesomeNeuralBrain;
import Brains.AwesomeNormalBrain;
import Logic.SnakeBrain;
import Util.awesome.threading.GA_IndividualThread;
import java.util.Collections;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/**
 * The Evolution Chamber contains the genetic algorithm. The main method
 * is the run simulation method. It contains the logic of the algorithm.
 * There is a population of AwesomeBrains that are initalized with random
 * Chromosoms. Then in a while loop for each individual in the population 
 * the fitness is determined by running games against given enemys. The 
 * AwesomeBrains that scored less are killed. With crossover the two best 
 * Brains create childs that fill the gaps of the killed ones. At last 
 * every Brain mutates. If the termination criteria is fullfilled the Algorithm
 * ends and the Best Chromosom is saved. If not the while loop continues with a
 * the new generation of individuals.
 * @author Chris
 */
class EvolutionChamber implements SimulationInterface{
    
    private LinkedList<AwesomeBrain> population;
    private int populationCount;
    // the amount of indivduums that are killed in a selection
    private int selectionKillCount;
    
    // how much the initialisation should derivate
    private int absoluteRange;
    private int gamesPerCombination;        
    
    // it counts amount of generations
    private int generationCounter;
    // how much generations the algorithm should produce
    private int generationLimit;    
    // if the fitness differ less than this limit also the
    // algorithm terminates
    private double dqLimit;
    
    private LinkedList <SnakeBrain> enemyBrains;
    // The className to determine if a neural Brain or an normal Brain runs
    String className;
    
/*----------------------------------------------------------------------------*/
//  CONSTRUCTORS
/*----------------------------------------------------------------------------*/ 
    /**
     * just default values for each attribut
     * @param className
     * @param enemyBrains 
     */
    public EvolutionChamber(String className, 
            LinkedList <SnakeBrain> enemyBrains){
        populationCount=10;
        selectionKillCount=populationCount/2;
        absoluteRange=10;
        gamesPerCombination=2;
        generationLimit=2;
        generationCounter=0;      
        dqLimit=0.001;
        this.enemyBrains=enemyBrains;
        this.className=className;
    }
    /**
     * all values are set by parameters
     * @param className
     * @param enemyBrains
     * @param populationCount
     * @param killRate
     * @param gamesPerCombination
     * @param generationLimit
     * @param dqLimit
     * @param absoluteRange 
     */
    public EvolutionChamber(String className, 
            LinkedList <SnakeBrain> enemyBrains, 
            int populationCount,
            double killRate,
            int gamesPerCombination,
            int generationLimit,
            double dqLimit,
            int absoluteRange){
        this.populationCount=populationCount;
        this.selectionKillCount=(int)(populationCount*killRate);
        this.absoluteRange=absoluteRange;
        this.gamesPerCombination=gamesPerCombination;
        generationCounter=0;      
        this.enemyBrains=enemyBrains;
        this.className=className;
        this.generationLimit=generationLimit;
        this.dqLimit=dqLimit;
    }
        
/*----------------------------------------------------------------------------*/
//  METHODS
/*----------------------------------------------------------------------------*/   
    /**
     * explained in introduction
     */
    public void runSimulation(){
        this.initializePopulation();
        do{
            System.out.println("Generation: "+ generationCounter);
            this.printPopulation();
            this.runGames();
            this.selection();
            this.crossover();
            this.mutation();            
            System.out.println("---------------------------------------------");
        }while(!isTerminating());
    }
    
    /**
     * prints every individual in a of the population with its fitness
     */
    public void printPopulation(){
        System.out.println("+++++++++++++++++++");
        for(int i = 0; i < population.size(); i++){
            System.out.println(i + 1 +  ". Snake fitness: \t " 
                    + population.get(i).getFitness());
        }
        System.out.println("+++++++++++++++++++");
    }
       
    /**
     * prints the attributes that the algorithm is started with
     */
    public void printSimulationInformation(){
        System.out.println("--------- |  EVOLUTIONCHAMBER  | ------------");
        System.out.println("Brain that is trained\t"+ className);
        System.out.println("---------------------------------------------");
        System.out.println("Population count \t"    + populationCount);
        System.out.println("Kills per selection \t" + selectionKillCount);
        System.out.println("Generation limit \t "   + generationLimit);
        System.out.println("Deviation quotient \t"  + dqLimit);
        System.out.println("Games per combination\t"+ gamesPerCombination);
        System.out.println("Games per Snake\t"+gamesPerCombination*enemyBrains.size());
        System.out.println("Games per Generation\t"
                +gamesPerCombination*enemyBrains.size()*populationCount);
        System.out.println("---------------------------------------------");
    }
    
    /**
     * prints the chromosom of the best snake
     */
    public void printSolution(){
        Collections.sort(population);
        AwesomeBrain first = population.getFirst();
        System.out.println("-------------------------------------------------");
        System.out.println("The Best Snake has the chromosom: ");
        first.printChromosom();        
        System.out.println("with Fitness of:\t " + first.getFitness());
    }
    
    /**
     * saves the chromosom of the best snake
     */
    public void saveSolution(){
        System.out.println("saving Chromosom to File...");
        // if the simulation running against a Single brain
        String s = "";
        if(enemyBrains.size() == 1){
            s += enemyBrains.getFirst().getClass().toString();
            s = s.replace("class Brains.", "");
        }
        else{
            s += "generalBrain";
        }
        population.getFirst().saveChromosom(s);
    }
    
    
/*----------------------------------------------------------------------------*/
//  initialize Population
/*----------------------------------------------------------------------------*/       
          
    private void initializePopulation(){
        this.initRandomPopulation();
    }
    
    /**
     * initialize a random population 
     */
    private void initRandomPopulation(){
        this.population = new LinkedList<>();
        for(int i=0; i<this.populationCount;i++){
            AwesomeBrain tmpBrain=null;
            if(this.className.contains("Neural") ){
                tmpBrain = new AwesomeNeuralBrain();
            }
            else if (this.className.contains("Normal")){
                tmpBrain = new AwesomeNormalBrain();
            }
            tmpBrain.createRandomChromosom(absoluteRange);
            this.population.add(tmpBrain);                        
        }                             
    }
    
    
/*----------------------------------------------------------------------------*/
//  run Games
/*----------------------------------------------------------------------------*/           
    
    /**
     * runs the simulation and determines the fitness of each brain.
     * For each individual a thread is generated where it competes against
     * the enemy brains. An individual thread spawns itself Threads in Runnable
     * Game. Over the thread the mean fitness is calculated.
     */
    private void runGames(){        
        long startTime = System.nanoTime();
        /* for first generation calculates for each AI the winrate and fitness
            for the following generations it is only necessary to calculate them
            for the newly created ones
        */ 
        Thread [] threadArr;
        if(generationCounter == 0){
            threadArr = new Thread[populationCount];
        }
        else{
            threadArr = new Thread[selectionKillCount];
        }
        
        // create the array and start the threads
        for(int i=0; i< threadArr.length; i++){
            threadArr[i] = new Thread(
                    new GA_IndividualThread(
                        i,
                        gamesPerCombination, 
                        population.get(population.size()-i-1), 
                        enemyBrains));
            threadArr[i].start();
        }  
        
        boolean allAlive = true;
        do{
            allAlive = false;
            for (Thread threadArr1 : threadArr) {
                if (threadArr1.isAlive()) {
                    allAlive = true;
                }               
            }
            // sleep for about 2 sec, that the processor is not on load
            try{Thread.sleep(2000);}catch(Exception e){}
        }while (allAlive);
        
        long endTime = System.nanoTime();
        long difference =  endTime - startTime ;
        System.out.println("time needed was: "
                + TimeUnit.NANOSECONDS.toSeconds(difference) + " sec oder "
                + TimeUnit.NANOSECONDS.toMinutes(difference) + " min.");
    }
    
    
/*----------------------------------------------------------------------------*/
//  Selection
/*----------------------------------------------------------------------------*/           
    
    /**
     * deletes the amount of selectionKillcount of snakes that are the weakest
     */
    private void selection(){
        // rank based on fitness
        Collections.sort(population);
        // remove selectionKillCount snakes from list
        for(int i=0; i < selectionKillCount; i++){
            population.removeLast();
        }  
    }
    
    
/*----------------------------------------------------------------------------*/
//  Crossover
/*----------------------------------------------------------------------------*/           

    /**
     * crossover between the 2 best Snakes from population.
     * add the childs till population size is reached again
     */
    private void crossover(){
        AwesomeBrain firstOfPop = population.get(0);
        AwesomeBrain tempBrain = firstOfPop.crossover(
                firstOfPop,
                population.get(1));
        for(int i=0; i < selectionKillCount; i++){
            population.add(tempBrain);
        }
    }

    
/*----------------------------------------------------------------------------*/
//  Mutation
/*----------------------------------------------------------------------------*/           
    
    /**
     * calls mutation in every brain. How mutation looks is defined in the 
     * brain classes
     */
    private void mutation(){
        for( AwesomeBrain brain : population){
            brain.mutation();
        }
    }

    
/*----------------------------------------------------------------------------*/
//  isTerminating
/*----------------------------------------------------------------------------*/     
    private boolean isTerminating(){        
        System.out.println(this.generationCounter);
        return terminationGenerationCount()
                || terminationStandardDeviation();
    }     
    
    /**
     * termination criterium is based on the count of the generations
     * @return 
     */
    private boolean terminationGenerationCount(){        
        generationCounter++;
        return generationCounter == generationLimit;
    }
    
    /**
     * calculate the deviation coefficient (dq) of the fitness of all snake and 
     * compares it to a set limit
     * @return  true if dq is lower than the limit
     */
    private boolean terminationStandardDeviation(){
        double relevantsize = population.size() - selectionKillCount;
        // calculate mean
        double mean=0;
        for(AwesomeBrain brain : population){
            mean += brain.getFitness();
        }
        mean = mean* (1/ relevantsize);
        
        // calculate deviation
        double dev=0;
        for(int i=0; i < relevantsize; i++){
            dev += Math.pow((population.get(i).getFitness() - mean), 2);
        }
        dev = dev * (1/relevantsize);
        
        //calculate Standard Deviation
        double sd =0;
        sd = Math.sqrt(dev);
        
        // calculate deviation koeffizient
        double dq = 0;
        dq = sd / mean;
        System.out.println("the mean fitness is "+ mean);
        System.out.println("the deviation is " + dev);
        System.out.println("the standard deviation is " + sd);
        System.out.println("the deviation coeffizient is " + dq);
        return (dq < this.dqLimit);            
    }     
}
