package Util.awesome.simulation;
import Brains.AwesomeNeuralBrain;
import Brains.AwesomeNormalBrain;
import Brains.HorstAI;
import Brains.NCageBrain;
import Brains.NotSoRandomBrain1;
import Brains.NotSoRandomBrain2;
import Brains.RandomBrain;
import Brains.SmartBrain;
import Brains.SuperBrain;
import Brains.WallBrain;
import Logic.SnakeBrain;
import java.util.LinkedList;

/**
 * This is the main method to run the simulations
 */
public class SimulateSnakes {
    public static void main(String[] args) {
//------------------------------------------------------------------------------        
//Init own classes
//        AwesomeNeuralBrain awesomNeural = new AwesomeNeuralBrain();
        AwesomeNormalBrain awesomeNormal = new AwesomeNormalBrain();
        awesomeNormal.setDebug(false);
//        awesomeNeural.setDebug(false);

//------------------------------------------------------------------------------        
// Initalize enemy Brains
        Brains.RandomBrain randBr = new RandomBrain();
        Brains.HorstAI horstBr = new HorstAI();
        Brains.NCageBrain nCageBr = new NCageBrain();
        Brains.NotSoRandomBrain1 notRandBr1 = new NotSoRandomBrain1();
        Brains.NotSoRandomBrain2 notRandBr2 = new NotSoRandomBrain2();
        Brains.SmartBrain smartBr = new SmartBrain();
        Brains.SuperBrain superBr = new SuperBrain();
        Brains.WallBrain wallBr = new WallBrain();
        
        LinkedList<SnakeBrain> enemyBrains = new LinkedList<>();
        enemyBrains.add(randBr);
//        enemyBrains.add(horstBr); // only sometimes exceptions
//        enemyBrains.add(nCageBr); // has output
//        enemyBrains.add(notRandBr1); // big matrix output
//        enemyBrains.add(notRandBr2); // throws exceptions
//        enemyBrains.add(superBr); // throws exceptions
        enemyBrains.add(wallBr);
//        enemyBrains.add(smartBr);
        

//------------------------------------------------------------------------------
//         BruteForce Simulation
//        BruteForce bruteForce = new BruteForce();
//        bruteForce.printSimulationInformation();
//        bruteForce.runSimulation();
//        bruteForce.printSolution();
//        System.out.println(awesomNeural.getName());


//------------------------------------------------------------------------------
//      Genetic Algorithm Simulation        
        EvolutionChamber evoChamb = new EvolutionChamber(
//                awesomNeural.getClass().getName(),      
                awesomeNormal.getClass().getName(),
                enemyBrains,                
                4,      // populationCount can't be under 4 
                0.25,   // killrate 
                1,      // games against each enemy
                2,      // generation Limit
                0.2,    // dqLimit
                100     // the range where the random values are assigned to the                         // chromosom Bsp.: 100 => -50 to +50
            );
        evoChamb.printSimulationInformation();
        evoChamb.runSimulation();
        evoChamb.printSolution();
        evoChamb.saveSolution();
   }
}
