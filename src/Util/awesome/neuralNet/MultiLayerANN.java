package Util.awesome.neuralNet;
import java.util.Random;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * MultiLayerANN this is a simple fedforward Multilayer neuronal net.
 * I wrote nearly all of the functions for a homework in "EInfuehrung in Neuronale
 * Netze" bei Zell. 
 */

public class MultiLayerANN implements java.io.Serializable{
    
	public static final Random rnd = new Random(System.currentTimeMillis());
	public static final double BIAS = 1;
        
	private int inputlayer;
	private int[] noninputlayer;
	private int noninputlayernum;

	private double[][] net;
	private double[][] act;
	private double[][] delta;
        
	private double[][][] weights;
	private double[][][] momentumTerm;

        
/*----------------------------------------------------------------------------*/
//  CONSTRUCTORS
/*----------------------------------------------------------------------------*/ 
	public MultiLayerANN(final int inputlayer, final int... noninputlayer) {
		//
		if (noninputlayer.length < 1) {
			throw new RuntimeException("At least an output layer must be given.");
		}
		//
		this.inputlayer = inputlayer;
		this.noninputlayer = noninputlayer;
		this.noninputlayernum = this.noninputlayer.length;
		//
		this.net = new double[this.noninputlayernum][];
		this.act = new double[this.noninputlayernum][];
		this.delta = new double[this.noninputlayernum][];
		//
		this.weights = new double[noninputlayernum][][];
		this.momentumTerm = new double[this.noninputlayernum][][];
		//
		int prevlayer = this.inputlayer;
		//
		for (int l = 0; l < this.noninputlayernum; l++) {
			int layer = this.noninputlayer[l];
			//
			this.net[l] = new double[layer];
			this.act[l] = new double[layer];
			this.delta[l] = new double[layer];
			//
			// +1 to consider also the on-neurons.
			//
			this.weights[l] = new double[prevlayer + 1][layer];
			this.momentumTerm[l] = new double[prevlayer + 1][layer];
			//
			prevlayer = layer;
		}
		//
		// initialize weights randomly.
		//
		for (int l = 0; l < this.weights.length; l++) {
			for (int i = 0; i < this.weights[l].length; i++) {
				for (int j = 0; j < this.weights[l][i].length; j++) {
					this.weights[l][i][j] = (rnd.nextDouble() * 0.2) - 0.1;
				}
			}
		}
	}
/*----------------------------------------------------------------------------*/
//  GETTER AND SETTER
/*----------------------------------------------------------------------------*/         
        public double [][][] getWheights(){
            return weights;
        }
        
        public void setWeights(double[][][] w) {
            this.weights = w;
        }
        
        public int getInputLayer(){
            return inputlayer;
        }
        
        public int [] getNonInputLayer(){
            return noninputlayer;
        }
        
        public int getNonInputLayerNum(){
            return noninputlayernum;
        }
        
        public String toString(){
            String s = "";
            s+= "Amount inputLayers: \t"+inputlayer +"\n";
            s+= "Amount nonInputLayers: \t"+noninputlayernum +"\n";
            s+= "Bias \t \t " + BIAS +"\n";
            return s;
        }

/*----------------------------------------------------------------------------*/
//  ALgorithm
/*----------------------------------------------------------------------------*/           
        /**
         * normal activation function
         * @param x
         * @return 
         */
	public static double act(final double x) {
		double result = 1 / (1 + Math.pow(Math.E, -x));
		return result;
	}
        /**
         * derivative of activation function
         * @param x
         * @return 
         */
	public static double actDx(final double x) {
		double result = act(x) * (1 - act(x));
		return result;
	}

	/**
	 * Calculates the net output for an input vector.
	 */
	public double[] predict(final double[] input) {
		// alte Aktivierungswerte l�schen
		for (int i = 0; i < noninputlayernum; i++) {
			for (int j = 0; j < noninputlayer[i]; j++) {
				net[i][j] = 0;
			}
		}

		//calc net and act for first noninputlayer
		for (int i = 0; i < noninputlayer[0]; i++) {
			for (int x = 0; x < input.length; x++) {

				net[0][i] = net[0][i] + input[x] * weights[0][x][i];
			}
			// On-Neuron
			//TODO add -1????
			net[0][i] = net[0][i] + BIAS * weights[0][input.length][i]; 
			act[0][i] = act(net[0][i]);
		}

		//calc net and act for all other noninputlayers
		for (int l = 1; l < noninputlayernum; l++) {
			for (int i = 0; i < noninputlayer[l]; i++) {
				for (int x = 0; x < noninputlayer[l - 1]; x++) {//l-1 to get the previous layer

					net[l][i] = net[l][i] + act[l - 1][x] * weights[l][x][i];
				}
				//TODO add -1???
				net[l][i] = net[l][i] + BIAS * weights[l][noninputlayer[l - 1]][i];
				act[l][i] = act(net[l][i]);
			}
		}

		return act[noninputlayernum - 1];
	}

	/**
	 * Trains the ANN for one input vector.
	 * @param input         training vector
	 * @param target
	 * @param learningrate
	 */
	private double trainpattern(final double[] input, final double[] target, final double learningrate, double momentum,
			double weightdecay) {
		this.predict(input);
		//
		double error = 0;

		// set deltas to 0
		for (int i = 0; i < noninputlayernum - 1; i++) {
			for (int j = 0; j < noninputlayer[i]; j++) {
				delta[i][j] = 0;
			}
		}

		// deltas for output layer
		for (int i = 0; i < noninputlayer[noninputlayernum - 1]; i++) {
//			System.out.println("act size = " + act.length);
//			System.out.println("nonnum = " + noninputlayernum);
//			System.out.println("target length = " +  target.length);
			if (act[noninputlayernum - 1][i] != target[i]) {
				error += Math.abs(act[noninputlayernum - 1][i] - target[i]);
			}
			delta[noninputlayernum - 1][i] = actDx(net[noninputlayernum - 1][i])
					* (target[i] - act[noninputlayernum - 1][i]);
		}

		// nonoutput layers (Backpropagation)
		for (int l = noninputlayernum - 2; l >= 0; l--) {//Layers
			for (int i = 0; i < noninputlayer[l]; i++) {//Neurons per Layer
				for (int x = 0; x < noninputlayer[l + 1]; x++) {//Neurons of Layer above
					//calc sum of weights * delta
					delta[l][i] = delta[l][i] + (delta[l + 1][x] * weights[l + 1][i][x]);
				}
				//multiply sum by actdx
				delta[l][i] = delta[l][i] * actDx(net[l][i]);
			}
		}

		//calculate weights:
		//weights from inputlayer to first noninputlayer
		for (int i = 0; i < inputlayer; i++) {
			for (int x = 0; x < noninputlayer[0]; x++) {

				
				double weightChange = learningrate * input[i] * delta[0][x]  
						+ momentum * momentumTerm[0][i][x]
						- weightdecay * weights[0][i][x];

				weights[0][i][x] = weights[0][i][x] + weightChange;
				momentumTerm[0][i][x] = weightChange;
			}
		}

		// weights
		for (int l = 1; l < noninputlayernum; l++) {
			for (int i = 0; i < noninputlayer[l - 1]; i++) {
				for (int x = 0; x < noninputlayer[l]; x++) {

					double weightChange = learningrate * act[l - 1][i] * delta[l][x]
							+ momentum * momentumTerm[l][i][x]
							- weightdecay * weights[l][i][x];
					weights[l][i][x] = weights[l][i][x] + weightChange;

					momentumTerm[l][i][x] = weightChange;
				}
			}
		}

		//calculate the thresholds (on-neuron)
		//input layer 
		for (int i = 0; i < noninputlayer[0]; i++) {

			double weightChange = learningrate *BIAS * delta[0][i] 
					+momentum * momentumTerm[0][inputlayer][i] 
					- weightdecay * weights[0][inputlayer][i];
					
			weights[0][inputlayer][i] = weights[0][inputlayer][i] + weightChange;

			momentumTerm[0][inputlayer][i] = weightChange;
		}

		//noninput layer
		for (int l = 1; l < noninputlayernum; l++) {
			for (int i = 0; i < noninputlayer[l]; i++) {
				
				double weightChange = learningrate * BIAS *delta[l][i] 
						+ momentum * momentumTerm[l][noninputlayer[l - 1]][i]
						- weightdecay * weights[l][noninputlayer[l - 1]][i];
				weights[l][noninputlayer[l - 1]][i] = weights[l][noninputlayer[l - 1]][i] + weightChange;

				momentumTerm[l][noninputlayer[l - 1]][i] = weightChange;
			}
		}
		return error / (double) (this.noninputlayer[this.noninputlayernum - 1]);
	}

	/**
	 * Trains the ANN for a set of input vector over a specified number of
	 * epochs.
	 *
	 * @param inputs
	 *            two dimensional array with training vectors. inputs[0] is the
	 *            first training vector, inputs[1] the second ..
	 * @param t
	 *            target outputs. t[0] is the desired output for the first
	 *            training vector.
	 * @param epochs
	 *            Number of iterations/epochs. One iteration corresponds to a
	 *            unique call of trainpattern for each training vector.
	 * @param learningrate
	 *
	 * @returns average error after/in last epoch
	 *
	 */
	public double train(final double[][] inputs, final double[][] targets, final int epochs, final double learningrate,
			double momentum, double weightdecay) {

		double error = 0;

		for (int i = 0; i < epochs; i++) {
			error = 0;
			for (int j = 0; j < inputs.length; j++) {
				error += this.trainpattern(inputs[j], targets[j], learningrate, momentum, weightdecay);
			}
			error = error / ((double) (inputs.length));
			//System.out.println("epoch nr. " + i + " " + "error: " + error);
		}

		System.out.println();
		System.out.println("final error: " + error);

		// evaluate the predictions
		int correctly_classified = 0;
		for (int i = 0; i < inputs.length; i++) {

			// For one output use thresholding with 0.5
			if (targets[i].length == 1)
				correctly_classified += Math.abs(this.predict(inputs[i])[0] - targets[i][0]) < 0.5 ? 1 : 0;

			// For more choose the output with the highest confidence as
			// predicted class
			else {
				double[] prediction = this.predict(inputs[i]);
				double maxvalue = 0;
				int maxindex = 0;
				for (int j = 0; j < prediction.length; j++) {
					if (maxvalue < prediction[j]) {
						maxindex = j;
						maxvalue = prediction[j];
					}
				}
				correctly_classified += targets[i][maxindex] == 1 ? 1 : 0;
			}
		}
		System.out.println("correctly classified: " + correctly_classified + "/" + inputs.length);
                System.out.println("Percentage: " + (((double) correctly_classified)/((double) inputs.length)));
		return error;
	}
        
        
        /**
         * Saves the Neuronalnet to a file with given filename
         * @param fileName 
         */
        public void saveObject(String fileName){            
            try{
                FileOutputStream f_out = new FileOutputStream(fileName);
                ObjectOutputStream obj_out = new ObjectOutputStream(f_out);
                obj_out.writeObject(this);
                obj_out.close();
                f_out.close();
            }
            catch(Exception e){
                System.out.println("couldn't create File");            
            }                        
        }
        
        /**
         * creates a neuronal net from a given file
         * @param fileName
         * @return 
         */
        public static MultiLayerANN readObject(String fileName){
            MultiLayerANN neuralNet = null;
            try{
                FileInputStream f_in = new FileInputStream(fileName);
                ObjectInputStream obj_in = new ObjectInputStream(f_in);
                Object obj = obj_in.readObject();
                if (obj instanceof MultiLayerANN){
                     neuralNet = (MultiLayerANN) obj;
                }
                else{
                    throw new Exception();
                }                    
            }
            catch(Exception e){
                System.out.println("couldn't read from File because of "+ e);
            }
            return neuralNet;
        }

}