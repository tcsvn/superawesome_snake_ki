package Util.awesome.neuralNet;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;

/** 
 * DataReader reads file into a double Matrix
 * Every new line is a new row in the double array 
 * the columns in a row are realized with seperating double values in a line with 
 * spaces
 */

public class DataReader {		
	public static double[][] readDoubleMatrix(final String filename) {
		double[][] res = null;
		try {
			int numberoflines = 0;
			BufferedReader reader = new BufferedReader(new FileReader(filename));			
			String line =reader.readLine();
			while (line != null) {
				numberoflines++;				
				line =reader.readLine();
			}
			reader.close();
			reader = new BufferedReader(new FileReader(filename));
			line =reader.readLine();
			res = new double[numberoflines][];
			int linenumber = 0;
			while (line != null) {
				StringTokenizer tokenizer = new StringTokenizer(line);			
				res[linenumber] = new double[tokenizer.countTokens()];
				int column = 0;
				while (tokenizer.hasMoreTokens()) {
					res[linenumber][column] = Double.parseDouble(tokenizer.nextToken());
					column++; 
				}
				linenumber++;
				line =reader.readLine(); 				
			}
			reader.close();			
		} catch (Exception e) {
			System.out.println("Error while reading " +  filename + "! Exception was: " + e.getMessage());
		}				
		return res;
	}
	
}
