package Util.awesome.neuralNet;

/**
 * This is the Main class that was used to create neural nets from either:
 * - inputArray + direction the snake moved patterns 
 *      was used to create an initial snake for the GA algorithm
 *      best result was 0.73 percent same moves as AwesomeNormalBrain
 * - inputArray + direction of enemySnake moved patterns
 *      should have been used to predict movement of enemy snake
 * - inputArray and direction snake took + type of snake
 *      should have been used to recognize against which snake one is playing
 * 
 * Here are some notes for different trials:
    //range weight-decay: 0.005-0.03
    //range momentum-term: 0.2-0.99
   
    // Problem of overfitting 
    // good values for KI LR=0.8; MT=0.5 Layers=20;20 => 0.751
    // good values for KI LR=0.9; MT=0.5 Layers=30;30 => 0.749
    // good values for KI LR=0.8; MT=0.4 Layers=20;20 => 0.748
    // good values for KI LR=0.9; MT=0.5 Layers=20;20 => 0.73       
    // good values for KI LR=0.8; MT=0.5 Layers=20;20;10 => 0.72
    // good values for KI LR=0.8; MT=0.7 Layers=20;20 => 0.59
    // good values for KI LR=0.8; MT=0.6 Layers=20;20;20 => 0.571                
    // good values for KI LR=0.9; MT=0.5 Layers=30;30;20;10 => 0.27
    // good values for KI LR=0./; MT=0.5 Layers=30;30;20;10 => 0.274     
 */

public class Main {
    
    public static void main(String [] args){
        args = new String[] {
            "awesomeBrain-input.txt",
            "awesomeBrain-target.txt",
            "10000",
            "0.8"
        };
        if (args.length != 4) {
                System.out.println("usage: MultiLayerANN <netinput-file> <teachingoutput-file> <iterations> <learningrate>");
                System.exit(1);
        }
        System.out.println("Reading in input File ...");
        final double[][] input = DataReader.readDoubleMatrix(args[0]);
        
        System.out.println("Reading in corresponding teaching Input ...");
        final double[][] teaching = DataReader.readDoubleMatrix(args[1]);
        final int iterations = Integer.parseInt(args[2]);
        final double learningrate = Double.parseDouble(args[3]);
//        final double weightdecay = Double.parseDouble(args[4]);
//        final double momentumTerm = Double.parseDouble(args[5]);

        // create MultiLayerANN with 2 hidden layer a 20 nodes
        System.out.println("Creating neural network...");
        MultiLayerANN ann = new MultiLayerANN(
                input[0].length,
                20,
                20,
                teaching[0].length);


        System.out.println("Training Network ....");        
        ann.train(input, teaching, iterations, learningrate,0.5,0);
        
        String name = "neural_InitBrain-For-GA.data";
        ann.saveObject(name);
        System.out.println(ann);
    }
}

