package Util.awesome.misc;
import Util.awesome.graph.GraphContainer;
import Brains.AwesomeBrain;
import static Brains.AwesomeBrain.round;
import Logic.GameInfo;
import Logic.Point;
import Logic.Snake;
import Logic.Snake.Direction;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * SnakePredictor 
 *  detects enemy Snakes based on neural net (not working)
 *  predicts a move of an enemy snake (not working)
 *  
 *  logs moves for own Snake and Enemy Snake for neural training (working) 
 * 
 * @author Chris
 */
public class SnakePredictor implements Serializable{
    // whether every move shall be logged or not
    protected final boolean logMovesOwnSnake;
    protected final boolean logMovesEnemySnake;
    
    private final String inputFileNameAwesome = "awesomeBrain-input.txt";
    private final String targetFileNameAwesome = "awesomeBrain-target.txt";
    private AwesomeBrain ownBrain;
    
    // stuff that I need for enemySnake logging
    private Point lastEnemyHeadPosition;
    private double [][] lastInput; 
    
    // 
    protected EnemySnake enemySnake = EnemySnake.GENERIC;
    protected boolean enemySnakeIdentified=false;   
    
    // our brain plays against the following snakes
    public enum EnemySnake{
        GENERIC,
        HORSTAI,
        NCageBrain,
        NotSoRandomBrain1,
        SuperBrain,
        WallBrain,
        RandomBrain        
    }   
/*----------------------------------------------------------------------------*/
//  CONSTRUCTORS
/*----------------------------------------------------------------------------*/    
    public SnakePredictor(
            AwesomeBrain ownBrain, 
            boolean logMovesEnemySnake,
            boolean logMovesOwnSnake){
        this.lastEnemyHeadPosition=new Point(2,7);
        this.ownBrain = ownBrain;
        this.logMovesEnemySnake=logMovesEnemySnake;
        this.logMovesOwnSnake=logMovesOwnSnake;
    }
    
 
/*----------------------------------------------------------------------------*/
//  Getter
/*----------------------------------------------------------------------------*/      
    private Point getEnemyHeadPosition(GameInfo gameInfo, Snake ownSnake){
        return ownBrain.getOtherSnake(ownSnake, gameInfo).headPosition();
    }
           
    public EnemySnake getEnemySnake(){
        return enemySnake;
    }

    
//-----------------------PREDICTI MOVE OF SNAKE---------------------------------
    
    
    
//----------------------------DETECTION OF ENEMY SNAKE--------------------------
    public void detectEnemySnake(Snake ownSnake, GameInfo gameInfo){
        if(!enemySnakeIdentified){
            enemySnakeIdentified = true;
            enemySnake = EnemySnake.GENERIC;            
        }
    }
    
    public Direction predictEnemySnakeNextMove(){
        return Direction.UP;
    }
    
    public double [] getAppropriateChromosom(){
        return null;
    } 
    /**
     * detects against which Snake one is playing and reloads the 
     * @param snake
     * @param gameInfo 
     */
    private void detectEnemySnakeOld(Snake snake, GameInfo gameInfo){
//        SnakeBrain enemy = ownBrain.getEnemySnake(snake, gameInfo).getBrain();
//        String enemyStr = enemy.getClass().toString();
        String enemyStr = "dummy";
        enemyStr = enemyStr.replace("class Brains.", "");
        switch (enemyStr){
            case "NCageBrain": {
                enemySnake=EnemySnake.NCageBrain;
                break;
            }
            case "NotSoRandomBrain1":{
                enemySnake=EnemySnake.NotSoRandomBrain1;
                break;
            }
            case "SuperBrain":{
                enemySnake=EnemySnake.SuperBrain;
                break;
            }
            case "WallBrain":{
                enemySnake=EnemySnake.WallBrain;
                break;
            }
            case "HorstAI":{
                this.enemySnake=EnemySnake.HORSTAI;
                break;
            }
            case "RandomBrain":{
                this.enemySnake=EnemySnake.RandomBrain;
                break;
            }
            default:{
                this.enemySnake=EnemySnake.GENERIC;
            }
        }        
        this.enemySnakeIdentified=true;
        ownBrain.setChromosom(ownBrain.loadChromosom());
    }
    
    
//------------------------------LOG MOVES OF SNAKES-----------------------------
        
    /**
     * write input and chosen direction to file to train an initial neural net for 
     * AwesomeBrain 
     * @param gameInfo 
     */    
    public void logMovesAwesomeSnake(Snake ownSnake, GameInfo gameInfo){
        if(logMovesOwnSnake){
            GraphContainer graphInfo = new GraphContainer(ownSnake.headPosition(), gameInfo.field());
            
            // get input
            double [][] input = ownBrain.getEveryInput(ownSnake, graphInfo, gameInfo); 
            writeArrayToFile(input, inputFileNameAwesome);
            
            // get directions
            double [] direcF = ownBrain.getScoreAllDirections(ownSnake, graphInfo, gameInfo);
            Direction d = ownBrain.getDirection(direcF[0], direcF[1], direcF[2], direcF[3]);
            double [] direc = createDirectionArr(d);                     
            writeArrayToFile(direc, targetFileNameAwesome);  
        }
    }
    
    /**
     * writes last input and the chosen direction to a file to train 
     * neural net to predict the moves of the snake
     * @param ownSnake
     * @param gameInfo 
     */
    public void logMovesEnemySnake(Snake ownSnake, GameInfo gameInfo){        
        if(logMovesEnemySnake){     
            Snake enemySnake = ownBrain.getOtherSnake(ownSnake, gameInfo);
            //get last move of enemy snake and write it to file
            Direction lastMove = getLastMovementOfSnake(ownSnake, gameInfo);
            double [] direc = createDirectionArr(lastMove);
            
            String direcStr  = getEnemySnake().toString() + "-target.txt";
            writeArrayToFile(direc, direcStr);
            
            String inputStr  = getEnemySnake().toString() + "-input.txt";
            writeArrayToFile(lastInput, inputStr);
            
            // save actual Headposition
            lastEnemyHeadPosition = enemySnake.headPosition();
            
            // get input for actual move for enemy snake and save it
            GraphContainer graphInfo = new GraphContainer(enemySnake.headPosition(), gameInfo.field());
            
            // todo or only for getInputArrayForDirection(null, snake, graphInfo, gameInfo)
            lastInput = ownBrain.getEveryInput(enemySnake, graphInfo, gameInfo);
        }
    }
    
    /**
     * gets the Last movement the enemy Snake. 
     * @param ownSnake
     * @param gameInfo
     * @return 
     */
    private Direction getLastMovementOfSnake(Snake ownSnake, GameInfo gameInfo){
        Point updatedHead = this.getEnemyHeadPosition(gameInfo, ownSnake);
        int diffX = lastEnemyHeadPosition.x - updatedHead.x;
        int diffY = lastEnemyHeadPosition.y - updatedHead.y;
        if(diffX > 0)       return Direction.LEFT;
        else if (diffX < 0) return Direction.RIGHT;
        else if(diffY > 0)  return Direction.UP;
        else if (diffY < 0) return Direction.DOWN;
        else{
            System.out.println("something went terrible wrong");
            return Direction.DOWN;
        }
    }
    
    /**
     * a given Direction is turned into four values. Each representing one 
     * direction. These values are later saved into a file where a neural net
     * can learn from it. 
     * Bsp.: Direction = UP -> [1.0, 0, 0 ,0]
     * @param d
     * @return 
     */
    private double [] createDirectionArr(Direction d){
            double [] direc =new double [4];
            if (null != d) switch (d) {
                 case UP:
                     direc[0] = 1.0;
                     break;
                 case RIGHT:
                     direc[1] = 1.0;
                     break;
                 case DOWN:
                     direc[2] = 1.0;
                     break;
                 case LEFT:
                     direc[3] = 1.0;
                     break;
                 default:
                     break;
             }
            for (int i = 0; i < direc.length; i++) {
                if (direc[i] != 1.0) {
                    direc[i] = 0;
                }
            }
            return direc;
    }

    
/*----------------------------------------------------------------------------*/
//  File Creation
/*----------------------------------------------------------------------------*/          
    /**
     * A given matrix is written to a file. The rows of the matrix are 
     * concatenated. Each entry ist seperated with a space. 
     * @param arr
     * @param fileName 
     */
    public void writeArrayToFile(double[][] arr, String fileName){
        try {
            // write input file
            List<String> inputLines = arrayToXSepStringList(arr, " ");
            File f = new File(fileName);
            f.createNewFile();
            Path inputFile = Paths.get(fileName);
            Files.write(
                    inputFile,
                    inputLines,
                    Charset.forName("UTF-8"),
                    StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.out.println("Something went wrong writing array to file!");
            System.out.println(e);
        }
    }
    
    /**
     * A given array is written to a file. Each entry is seperated with a space
     * @param arr
     * @param fileName 
     */
    public void writeArrayToFile(double[] arr, String fileName){
        try {
            // write input file
            List<String> inputLines = arrayToXSepStringList(arr, " ");
            File f = new File(fileName);
            f.createNewFile();
            Path inputFile = Paths.get(fileName);
            Files.write(
                    inputFile,
                    inputLines,
                    Charset.forName("UTF-8"),
                    StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.out.println("Something went wrong writing array to file!");
            System.out.println(e);
        }
    }

    /**
     * Turns a matrix into a String. Each entry is seperated by the String
     * seperator.
     * @param arr
     * @param seperator
     * @return 
     */  
    private static List<String> arrayToXSepStringList(double[][] arr, String seperator) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                double d = round(arr[i][j], 5);
                list.add(Double.toString(d));
            }
        }
        String line = "";
        for (String s : list) {
            line += s + seperator;
        }
        List<String> lines = new ArrayList<>();
        lines.add(line);
        return lines;
    }    
    
     /**
     * changes an double array to a single String. Each entry is seperated by
     * the given seperator
     *
     * @param arr
     * @param seperator which seperator is used for a line in the list (bsp.: "
     * " space or "," for comma seperated string list
     * @return
     */
    private static List<String> arrayToXSepStringList(double [] arr, String seperator) {
        // convert double to String array
        String[] arr2 = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            // round to 5 digits precision and convert to string
            double d = round(arr[i], 5);

            arr2[i] = Double.toString(d);
        }
        List<String> list = Arrays.asList(arr2);
        String line = "";
        for (String s : list) {
            line += s + seperator;
        }
        List<String> lines = new ArrayList<>();
        lines.add(line);
        return lines;
    }
}
