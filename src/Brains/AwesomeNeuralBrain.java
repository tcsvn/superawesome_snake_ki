package Brains;
import Logic.GameInfo;
import Logic.Snake;
import Util.awesome.graph.GraphContainer;
import Util.awesome.neuralNet.MultiLayerANN;
import java.util.Random;

/**
 * AwesomeNeuralBrain uses a Multilayer Neural net to determine the scores 
 * for each direction. 
 * 
 */

public class AwesomeNeuralBrain extends AwesomeBrain{    
    /*
        the following Strings are the specific neural nets for each Enemy and
        a generic one for all. The neural nets are encoded in the files.
    */
    private final String filename_Generic=
            "neural_InitBrain-For-GA.data";
    private final String filename_HorstAI=
            "chromosomAwesomeNeuralBrain-Jul_10_12.45.55Uhr-WR_0.0-FIT_100.0-RandomBrain.data";
    private final String filename_NCageBrain=
            "chromosomAwesomeNeuralBrain-Jul_10_12.45.55Uhr-WR_0.0-FIT_100.0-RandomBrain.data";
    private final String filename_RandomBrian=
            "chromosom-AwesomeNeuralBrain-Jul_10_20.11.55Uhr-WR_0.0-FIT_50.0-RandomBrain.data";
    private final String filename_NotSoRandomBrain1=
            "chromosomAwesomeNeuralBrain-Jul_10_12.45.55Uhr-WR_0.0-FIT_100.0-RandomBrain.data";
    private final String filename_SuperBrain=
            "chromosomAwesomeNeuralBrain-Jul_10_12.45.55Uhr-WR_0.0-FIT_100.0-RandomBrain.data";
    private final String filename_WallBrain=
            "chromosomAwesomeNeuralBrain-Jul_10_12.45.55Uhr-WR_0.0-FIT_100.0-RandomBrain.data";

    private MultiLayerANN neuralNet;    
    
/*----------------------------------------------------------------------------*/
//  CONSTRUCTORS
/*----------------------------------------------------------------------------*/ 

    
    public AwesomeNeuralBrain(){                
        /* false otherwise, loadChromosom is called in Parent but the String 
            filename variables are not initialized yet
        */
        super(false);
        this.neuralNet = MultiLayerANN.readObject(filename_Generic);
    }
    public AwesomeNeuralBrain(double [] chrom){                
        super(chrom);
        this.neuralNet = MultiLayerANN.readObject(filename_Generic);
    }
    
   
/*----------------------------------------------------------------------------*/
//  Genetic Algorithm
/*----------------------------------------------------------------------------*/   
    /**
     * the methos saves the neural net into a file
     */
    @Override
    public void saveChromosom(){
        String filename = createFilename(this.getClass().toString());
        filename = filename.replace("class Brains.", "");
        filename += ".data";
        neuralNet.saveObject(filename);
    }
    /**
     * same as above, but a postfix can be appended to the filename
     * @param postfix 
     */
    @Override
    public void saveChromosom(String postfix){
        String filename = createFilename(this.getClass().toString());
        filename = filename.replace("class Brains.", "");
        filename += "-" + postfix;
        filename += ".data";
        neuralNet.saveObject(filename);
    }
    
    
    /**
     * selects the neural net that should be loaded for an opponent based 
     * on what snakePredictor predicts. It loads the neural net and converts
     * it to a chromsom which it returns.
     * @return 
     */
    @Override
    public double[] loadChromosom() {    
        switch(snakePredictor.getEnemySnake()){
            case GENERIC:{
                neuralNet = MultiLayerANN.readObject(filename_Generic);
                break;
            }
            case HORSTAI:{
                neuralNet = MultiLayerANN.readObject(filename_HorstAI);
                break;
            }
            case  NCageBrain:{
                neuralNet = MultiLayerANN.readObject(filename_NCageBrain);
                break;        
            }
            case NotSoRandomBrain1:{
                neuralNet = MultiLayerANN.readObject(filename_NotSoRandomBrain1);
                break;
            }
            case RandomBrain:{
                neuralNet = MultiLayerANN.readObject(filename_RandomBrian);
                break;
            }
            case SuperBrain:{
                neuralNet = MultiLayerANN.readObject(filename_SuperBrain);
                break;
            }   
            case WallBrain:{
                neuralNet = MultiLayerANN.readObject(filename_WallBrain);
                break;
            }                    
            default:{
                neuralNet = MultiLayerANN.readObject(filename_Generic);
            }
        }    
        return wheightsToChromosom(neuralNet);
    }
    
    /**
     * creates a random Chromosom and writes the values converts the chromosom
     * to a neural net
     * @param absoluteRange 
     */
    @Override
    public void createRandomChromosom(double absoluteRange){
        Random randgen = new Random();
        chromosom = loadChromosom();
        for(int i = 0; i < chromosom.length; i++){
            chromosom[i] = absoluteRange-randgen.nextDouble()*2*absoluteRange;
        }
        neuralNet.setWeights(chromosomToWheights(neuralNet, chromosom));
    }     
    
    /**
     * the method is given an encoded neural net and converts it to a chromosom
     * @param neuralNet
     * @return 
     */
    private double [] wheightsToChromosom(MultiLayerANN neuralNet){
        // Calculate the sum 
        int sum = 0;        
        // first wheights [0]  inputlayer(24)* 20 gewichte (20 weil naechste schicht 20 hat)
        int amountWheightInputToFirstHidden =neuralNet.getInputLayer()+1;
        int amountFirstHiddenLayer = neuralNet.getNonInputLayer()[0];
        int amountHiddenLayers=neuralNet.getNonInputLayerNum()-1;
        int [] nonInputLayer = neuralNet.getNonInputLayer();
        
        sum+= amountWheightInputToFirstHidden*amountFirstHiddenLayer;
        for(int i=0; i<amountHiddenLayers;i++){
            sum+= (neuralNet.getNonInputLayer()[i]+1)*neuralNet.getNonInputLayer()[i+1];
        }
        
        // new method ;)
        int size = sum;
        double [] chrom = new double [size];
        double [][][] wheights = neuralNet.getWheights();
        int l = 0;        
        //first inputlayer
        for(int i =0; i<amountWheightInputToFirstHidden;i++){
            for(int j =0; j < amountFirstHiddenLayer; j++){                
                chrom[l] = wheights[0][i][j];
                l++;
            }
        }
        //all other hiddenlayers
        for(int i=1; i < 1+amountHiddenLayers; i++){
            int amountThisLayer=1+nonInputLayer[i-1];
            for(int j=0; j < amountThisLayer; j++){
                int amountNextLayer=nonInputLayer[i];
                for(int k=0; k < amountNextLayer; k++){
                    chrom[l]=wheights[i][j][k];
                    l++;
                }
            }
        }   
        return chrom;
    }    
    
    /**
     * copys the values of a given chromosom to the values of a given neural net
     * @param neuralNet
     * @param chrom
     * @return 
     */
    private double [][][] chromosomToWheights(MultiLayerANN neuralNet, double [] chrom){
        int amountWheightInputToFirstHidden =neuralNet.getInputLayer()+1;
        int amountFirstHiddenLayer = neuralNet.getNonInputLayer()[0];
        int amountHiddenLayers=neuralNet.getNonInputLayerNum()-1;
        int [] nonInputLayer = neuralNet.getNonInputLayer();        
        double [][][] wheights = neuralNet.getWheights();
        int l = 0;        
        
        //first inputlayer
        for(int i =0; i<amountWheightInputToFirstHidden;i++){
            for(int j =0; j < amountFirstHiddenLayer; j++){                
                wheights[0][i][j] = chrom[l];
                l++;
            }
        }
        //all other hiddenlayers
        for(int i=1; i < 1+amountHiddenLayers; i++){
            int amountThisLayer=1+nonInputLayer[i-1];
            for(int j=0; j < amountThisLayer; j++){
                int amountNextLayer=nonInputLayer[i];
                for(int k=0; k < amountNextLayer; k++){
                    wheights[i][j][k]= chrom[l];
                    l++;
                }
            }
        }   
        return wheights;
    }
    
    /**
     * calculates a string representation of the neural net
     * used to check if mutation method in evolution chamber works correctly by 
     * calling this method before and after mutation
     * @param neuralNet
     * @param chrom
     * @return 
     */
    public double calculateHashForWheights(MultiLayerANN neuralNet, double [] chrom){
        int amountWheightInputToFirstHidden =neuralNet.getInputLayer()+1;
        int amountFirstHiddenLayer = neuralNet.getNonInputLayer()[0];
        int amountHiddenLayers=neuralNet.getNonInputLayerNum()-1;
        int [] nonInputLayer = neuralNet.getNonInputLayer();        
        double [][][] wheights = neuralNet.getWheights();
        double hash=0.0;
        
        //first inputlayer
        for(int i =0; i<amountWheightInputToFirstHidden;i++){
            for(int j =0; j < amountFirstHiddenLayer; j++){                
                hash += wheights[0][i][j];
            }
        }
        //all other hiddenlayers
        for(int i=1; i < 1+amountHiddenLayers; i++){
            int amountThisLayer=1+nonInputLayer[i-1];
            for(int j=0; j < amountThisLayer; j++){
                int amountNextLayer=nonInputLayer[i];
                for(int k=0; k < amountNextLayer; k++){
                    hash += wheights[i][j][k];
                }
            }
        }   
        return hash;
    }
    
    /**
     * self explanatory
     */
    @Override
    public void printChromosom(){        
        System.out.println(calculateHashForWheights(neuralNet, chromosom));
    }

    /**
     * uses arithmetic recombination for a crossover
     * is better than uniform crossover because a wheight that connects two 
     * neurons is no meaningful entity alone
     * @param a
     * @param b
     * @return 
     */
    @Override
    public AwesomeBrain crossover(AwesomeBrain a, AwesomeBrain b) {
        double[] childchromosom = arithmeticRecombination(
                a.getChromosom(),
                b.getChromosom(),
                0.2);
        return new AwesomeNeuralBrain(childchromosom);
    }
    
    /**
     * uses the SDMutation to mutate all wheights
     */
    @Override
    public void mutation() {
        // mutate chromosom
        super.SDMutation(0.3);
        // load chromsom into Neural Net wheights
        neuralNet.setWeights(this.chromosomToWheights(neuralNet, chromosom));
    }

/*----------------------------------------------------------------------------*/
//  Direction Explicit Implem
/*----------------------------------------------------------------------------*/    
    
    @Override
    public double [] getScoreAllDirections(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        // calculate Array for each Direction
        double [][] multiDirec = getEveryInput(snake, graphInfo, gameInfo);
        
        // Append arrays
        double [] input = appendArrays(multiDirec);
        
        // let neural net predict direction        
        return neuralNet.predict(input);
    }
    
    /**
     * append each inputArray for each direction to one big Array that is used
     * for as input for the neural net
     * @param direc
     * @return 
     */
    private double [] appendArrays(double [][] direc){
        int inputLayer = this.neuralNet.getInputLayer();        
        double[] input = new double[inputLayer];
        double row0 = input.length * 0.25;
        double row1 = input.length * 0.5;
        double row2 = input.length * 0.75;
        for (int i = 0; i < input.length; i++) {                  
            int index = (int) (i % row0);
            if (i < row0) {
                input[i] = direc[0][index];
            } else if (i < row1) {
                input[i] = direc[1][index];
            } else if (i < row2) {
                input[i] = direc[2][index];
            } else {
                input[i] = direc[3][index];
            }
        }
        return input;
    }    


}
