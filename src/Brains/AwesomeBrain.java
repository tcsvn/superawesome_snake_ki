package Brains;

import java.util.Random;
import Logic.Field.CellType;
import Logic.GameInfo;
import Logic.Point;
import Logic.Snake;
import Logic.Snake.Direction;
import Logic.SnakeBrain;
import Util.awesome.graph.GraphContainer;
import Util.awesome.graph.GraphContainer.GraphType;
import Util.awesome.graph.SnakeExt;
import Util.awesome.misc.SnakePredictor;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.SerializationUtils;
import Util.awesome.threading.DirectionThread;

;

/**
 * AwesomeBrain is an (awesome) AI for Snake. 
 * For each direction an an inputArray is calculated with parameters that 
 * contain information about the gameField. Then a score is calculated for each 
 * direction based on the inputArrays either with a neural net or with a 
 * value function by the inhereting classes. The direction with the maximal 
 * score is used for the next move. 
 *
 * Genetic Propertys of an AwesomeBrain
 * the chromosom consists of genes that are represented by double fields in the
 * array chromosom. These values determine the behaviour of the snake. 
 * In the case of an AwesomeNormalBrain the chromosom encodes factors the results
 * of the input functions are multiplicated by. 
 * In the case of the AwesomeNeuralBrain the chromosom encodes the wheights of 
 * the neural net. 
 * Each Snake inherits some methods that it has to override and that are used 
 * in util.awesome.simulation.evolutionchamber.
 * For more information about the genetic algorithm look there. 
 *
 * @author Flo , Chris
 *
 */
public abstract class AwesomeBrain extends Thread implements SnakeBrain, Comparable<AwesomeBrain>, Serializable {

    // false leads to suppression of the output
    protected boolean debug = false;
    // a value, that measures how good an AwesomeBrain is compared to other AwesomeBrains
    private double fitness;
    // arr. contains all parameters, that can be changed for a snake
    protected double[] chromosom;
    // the length of chromosom
    protected int geneCount;
    // the count of functions where an input for a direction is derived
    private final int amountOfFunctionsUsedForInput = 12;

    // the amount of ticks the snake has made
    protected int gameTicks = 0;

    // predicts which snake is playing and logs their behaviour
    protected SnakePredictor snakePredictor;

/*----------------------------------------------------------------------------*/
//  CONSTRUCTORS
/*----------------------------------------------------------------------------*/
    /**
     * normal standard constructor
     */
    public AwesomeBrain() {
        snakePredictor = new SnakePredictor(this, false, false);
        this.fitness = 0;
        this.chromosom = loadChromosom();
        this.geneCount = chromosom.length;

    }

    /**
     * this constructor is used in the evolutionary simulation for more
     * information refer to Util.awesome.simulation.evolutionchamber
     *
     * @param loadChromosom
     */
    public AwesomeBrain(boolean loadChromosom) {
        snakePredictor = new SnakePredictor(this, false, false);
        fitness = 0;
        if (loadChromosom) {
            chromosom = null;
            geneCount = 0;
        } else {
            this.chromosom = loadChromosom();
            this.geneCount = chromosom.length;
        }
    }

    /**
     * This constructor creates a AwesomeSnake with a given chromosom
     *
     * @param chromosom
     */
    public AwesomeBrain(double[] chromosom) {
        snakePredictor = new SnakePredictor(this, false, false);
        this.geneCount = chromosom.length;
        this.chromosom = chromosom;
    }

    // Getter and Setter
    public void setFitness(double fit) {
        this.fitness = fit;
    }

    public double getFitness() {
        return this.fitness;
    }

    public void setGeneCount(int count) {
        this.geneCount = count;
    }

    public double[] getChromosom() {
        return this.chromosom;
    }

    public void setChromosom(double[] chromosom) {
        this.chromosom = chromosom;
    }

    public void setDebug(boolean deb) {
        this.debug = deb;
    }

/*----------------------------------------------------------------------------*/
//  Genetic Algorithm
/*----------------------------------------------------------------------------*/
    // for comments look into the child classes
    public abstract void printChromosom();

    public abstract void saveChromosom();

    public abstract void saveChromosom(String postfix);

    public abstract double[] loadChromosom();

    public abstract AwesomeBrain crossover(AwesomeBrain a, AwesomeBrain b);

    public abstract void mutation();

/*----------------------------------------------------------------------------*/
//  Initialisation
    /**
     * @param absoluteRange determines the range where a gene (field of array)
     * has ist value
     * Bsp.: if absR = 10 the value the chromosom[j] lays between -10 and 10
     *
     */
    public void createRandomChromosom(double absoluteRange) {
        Random randGen = new Random();
        for (int j = 0; j < chromosom.length; j++) {
            chromosom[j] = absoluteRange - randGen.nextDouble() * 2 * absoluteRange;
        }
    }

/*----------------------------------------------------------------------------*/
//  Crossover
    /**
     * Uniform Crossover
     * A chromosom of a child is created by selecting a gene from two parents
     * based on a random Value. The value can be shifted to favor the genes
     * of a parent over another
     * @param parent1
     * @param parent2
     * @param offset is the parameter that favors parent1 over parent2 [0.0,
     * 0.5]
     * @return the childs chromosom
     */
    public static double[] uniformCrossover(double[] parent1, double[] parent2, double offset) {
        Random randGen = new Random();
        double[] child = new double[parent1.length];
        for (int gen = 0; gen < parent1.length; gen++) {
            double wk = randGen.nextDouble();
            wk = wk + offset;
            if (wk > 0.5) {
                child[gen] = parent1[gen];
            } else {
                child[gen] = parent2[gen];
            }
        }
        return child;
    }

    /**
     * Arithmetic Recombination
     * the values of each gene are added to onto each other. The wheight determines
     * which gene of a parent is more valued.
     * @param parent1
     * @param parent2
     * @param wheight   if high parent1 gene is more valued than parent2s gene
     * @return the childs chromosom
     */
    public static double[] arithmeticRecombination(double[] parent1, double[] parent2, double wheight) {
        double[] child = new double[parent1.length];
        for (int gen = 0; gen < child.length; gen++) {
            double childgen;
            childgen = parent1[gen] * wheight + (1 - wheight) * parent2[gen];
            child[gen] = childgen;
        }
        return child;
    }

/*----------------------------------------------------------------------------*/
//  Mutation
    
    /**
     * in this method a random amount of genes are mutated by the flipOneBit
     * mutation
     */
    public void bitFlipMutation() {
        Random randGenerator = new Random();
        int randCount = randGenerator.nextInt(this.geneCount);
        // flips random amount of bits in chromosom
        for (int i = 0; i < randCount; i++) {
            this.flipOneBit();
        }
    }

    /**
     * as for non double values each 1 or 0 would represent an phaene (property
     * of individuum) a bitflip would change that property. For our case
     * we have double values this is just a random change to a gene
     */
    private void flipOneBit() {
        Random randGenerator = new Random();
        byte maske = (byte) ((byte) 2 ^ (randGenerator.nextInt(8)));
        int randGen = randGenerator.nextInt(this.geneCount);
        // gen has a 1 at position
        if (((byte) chromosom[randGen] & (byte) maske) > 0) {
            // set gen at position maske 0
            maske = (byte) ~maske;
            chromosom[randGen] = (byte) ((byte) chromosom[randGen] & (byte) maske);
        } else {
            // set gen at position maske 1
            chromosom[randGen] = (byte) ((byte) chromosom[randGen] | (byte) maske);
        }
    }

    /**
     * in this method a random amount of genes are mutated by the SDMutate mutation
     * @param sd
     */
    public void SDMutation(double sd) {
        Random randGenerator = new Random();
        //amout of gene that should mutate
        int randCount = randGenerator.nextInt(this.geneCount);
        for (int i = 0; i < randCount; i++) {
            int geneToMutate = randGenerator.nextInt(this.geneCount);
            SDMutate(sd, geneToMutate);
        }
    }
    
    private void SDMutate(double sg, int gene) {
        Random randGenerator = new Random();
        double sd = chromosom[gene] * sg;
        if (randGenerator.nextDouble() > 0.5) {
            chromosom[gene] += sd;
        } else {
            chromosom[gene] -= sd;
        }
    }

/*----------------------------------------------------------------------------*/
//  Comparision    
    /**
     * compares a given awesomeBrain based on fitness with itself. Is used for
     * sorting the Arraylist in the selection part in 
     * util.awesome.simulation.evolutionchamber
     *
     * @param o
     * @return -1 if the given object is less valued than self, 0 if equal and
     * positive if the given object is more of value than self
     */
    @Override
    public int compareTo(AwesomeBrain o) {
        if (this.getFitness() == o.getFitness()) {
            return 0;
        } else if (this.getFitness() < o.getFitness()) {
            return 1;
        } else {
            return -1;
        }
    }


    
    
/*----------------------------------------------------------------------------*/
//  Input Array Calculation
/*----------------------------------------------------------------------------*/
    
    
/*----------------------------------------------------------------------------*/
//  Area of Enemy or Own Snake
    
    /**
     * the lesser the possibilities to move for the Enemy the better for us the
     * diameter of the enemy snake to the farest position it can go. the lesser
     * the diameter the better for us
     *
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double maxStepsEnemy(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        Snake enemy = getOtherSnake(snake, gameInfo);
        Point from = enemy.headPosition();
        int maxSteps = graphInfo.calcMaxStepsPossibleFrom(from, GraphType.ONLY_APPLE);
        /* the factor should have more wheight if the step reduces the enemy
         area of direction
         */
        this.displayDebug("max amount of steps Enemy can take: " + maxSteps);
        return this.scaleLowerStepsHigherImpact(maxSteps);
    }

    /**
     * calculates the maximal steps the Own snake has left to move in the field
     *
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double maxStepsOwnSnake(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        Point from = snake.headPosition();
        int maxSteps = graphInfo.calcMaxStepsPossibleFrom(from, GraphType.ONLY_APPLE);
        /* the factor should have more wheight if the step reduces the enemy
         area of direction
         */
        this.displayDebug("max amount of steps Awesome can take: " + maxSteps);
        return this.scaleHigherStepsHigherImpact(maxSteps);
    }

/*----------------------------------------------------------------------------*/
//  Calculation of Distances to Objects
    
    /**
     * calculates the distance to the last segment of the tail of a snake
     *
     * @param snake
     * @return
     */
    public double distanceToOwnTail(Snake snake) {
        Point tail = snake.segments().getFirst();
        double dist = calcPointDistance(tail, snake.headPosition());
        this.displayDebug("euclidian Distance: " + dist);
        return dist;
    }

    /**
     * calculates the distance to the head of a snake
     *
     * @param snake
     * @param gameInfo
     * @return
     */
    public double distanceToHeadEnemy(Snake snake, GameInfo gameInfo) {
        Snake enemy = getOtherSnake(snake, gameInfo);
        double distance = calcPointDistance(enemy.headPosition(), snake.headPosition());
        this.displayDebug("euclidian Distance: " + distance);
        return 1 / distance;
    }

    /**
     * calculates the euclidian distance between the head of the snake and the apple if
     * there is an apple in the field;
     * mainly used for debugging
     *
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double distanceToApple(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        boolean appleFound = false;
        // get point of apple
        Point applePos = new Point(0, 0);
        for (int i = 0; i < gameInfo.field().width(); i++) {
            for (int j = 0; j < gameInfo.field().height(); j++) {
                if (gameInfo.field().cell(new Point(i, j)) == CellType.APPLE) {
                    applePos = new Point(i, j);
                    appleFound = true;
                    break;
                }
            }
        }
        if (!appleFound) {
            return 0.0;
        } else {
            double distance = calcPointDistance(applePos, snake.headPosition());
            this.displayDebug("euclidian Distance: " + distance);
            return 1 / distance;
        }
    }

    /**
     * A snake is given to and the other snake in the game is returned
     * is sometimes used to get the enemys snake
     *
     * @param snake
     * @param gameInfo
     * @return
     */
    public Snake getOtherSnake(Snake snake, GameInfo gameInfo) {
        Snake snake1 = gameInfo.snakes().get(0);
        Snake snake2 = gameInfo.snakes().get(1);
        if (snake1.headPosition().equals(snake.headPosition())) {
            return snake2;
        } else {
            return snake1;
        }
    }

/*----------------------------------------------------------------------------*/
//  calc if Block is Hostile

    /**
     * simulates head movement in the given direction for a given snake and
     * checks if that move is possible(not a wall or a snake) returns true if
     * possible
     *
     *
     * @param d
     * @param snake
     * @param gameInfo
     * @return
     */
    private boolean isMoveValid(Direction d, Snake snake, GameInfo gameInfo) {
        Point newHead = simulateHead(d, snake);
        if (newHead.x == -1) {
            newHead.x = gameInfo.field().width() - 1;
        }
        if (newHead.x == gameInfo.field().width()) {
            newHead.x = 0;
        }
        if (newHead.y == -1) {
            newHead.y = gameInfo.field().height() - 1;
        }
        if (newHead.y == gameInfo.field().height()) {
            newHead.y = 0;
        }

        return gameInfo.field().cell(newHead) == CellType.SPACE
                || gameInfo.field().cell(newHead) == CellType.APPLE
                || gameInfo.field().cell(newHead) == CellType.FEATUREWALL
                || gameInfo.field().cell(newHead) == CellType.PORTAL
                || gameInfo.field().cell(newHead) == CellType.CHANGEHEADTAIL
                || gameInfo.field().cell(newHead) == CellType.CHANGESNAKE
                || gameInfo.field().cell(newHead) == CellType.CUTTAIL
                || gameInfo.field().cell(newHead) == CellType.OPENFIELD
                || gameInfo.field().cell(newHead) == CellType.OPENFIELDPICTURE
                || gameInfo.field().cell(newHead) == CellType.SPEEDUP;
    }

    /**
     * checks if the snake can at least make one valid move
     *
     * @param snake
     * @param gameInfo
     * @return
     */
    private boolean isValidMovePossible(Snake snake, GameInfo gameInfo) {
        return isMoveValid(Direction.DOWN, snake, gameInfo)
                || isMoveValid(Direction.UP, snake, gameInfo)
                || isMoveValid(Direction.LEFT, snake, gameInfo)
                || isMoveValid(Direction.RIGHT, snake, gameInfo);
    }

/*----------------------------------------------------------------------------*/
//  Steps to Apple    
    /**
     * the function returns the shortest path to the nearest apple 
     *
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double calcStepsToApple(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        if (graphInfo.selectGraph(GraphType.ONLY_APPLE).containsVertexWithCellType(CellType.APPLE)) {
            int stepsToApple = graphInfo.calcStepsToNearestCellType(
                    snake.headPosition(),
                    CellType.APPLE,
                    GraphType.ONLY_APPLE);
            this.displayDebug("amount of steps: " + stepsToApple);
            return this.scaleLowerStepsHigherImpact(stepsToApple);
        }
        displayDebug("no Apple on Map");
        return 0.0;
    }

/*----------------------------------------------------------------------------*/
// Steps to Portal
    /**
     * calculates the steps to the nearest portal
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double calcStepsToFeature_Portal(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        if (gameInfo.getPortal().isActive()) {
            int stepsToNearestPortal;
            stepsToNearestPortal = graphInfo.calcStepsToNearestCellType(snake.headPosition(),
                    CellType.PORTAL,
                    GraphType.ONLY_PORTALS);
            this.displayDebug("amount of steps: " + stepsToNearestPortal);
            return this.scaleLowerStepsHigherImpact(stepsToNearestPortal);
        } else {
            this.displayDebug("no Portal on map");
            return 0.0;
        }
    }

/*----------------------------------------------------------------------------*/
//  Wall Feature
    /**
     * decides whether the Wall is set between enemy and the apple or the middle
     * of the field
     *
     * @param snake
     * @param graphInfo
     * @param gameInfo
     */
    public void setWall(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {

        Point apple = graphInfo.getApplePos();

        if (apple != null) {
            setWallBetweenPoint(snake, graphInfo, gameInfo, apple);

        } else {
            Point middle = new Point(gameInfo.field().width() / 2, gameInfo.field().height() / 2); // Point in the middle of the Field
            setWallBetweenPoint(snake, graphInfo, gameInfo, middle);
        }

    }

    /**
     *	calculates the position of the feature Wall 
     *	the function calculates the direction oft the given point (apple or middle of the field)
     *	from the view of the head of the enemy snake and sets the wall one field in that direction. 
     * 
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @param point
     */
    private void setWallBetweenPoint(Snake snake, GraphContainer graphInfo, GameInfo gameInfo, Point point) {

        Point enemyHead = getOtherSnake(snake, gameInfo).headPosition();
        Point setPoint;
        Direction direction;

        if (enemyHead.x < point.x) {
            if (enemyHead.y < point.y) {
                setPoint = new Point(enemyHead.x + 1, enemyHead.y + 1);
                direction = Direction.LEFT;
            } else {
                setPoint = new Point(enemyHead.x + 1, enemyHead.y - 1);
                direction = Direction.UP;
            }
        } else {
            if (enemyHead.y < point.y) {
                setPoint = new Point(enemyHead.x - 1, enemyHead.y + 1);
                direction = Direction.RIGHT;
            } else {
                setPoint = new Point(enemyHead.x - 1, enemyHead.y - 1);
                direction = Direction.DOWN;
            }
        }
        snake.setWall(setPoint, direction);
    }

    /**
     * checks if there is a FeatureWall Field and calculates the steps to it
     *
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double calcStepsToFeature_Wall(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        if (gameInfo.field().hasFeatureWall()) {
            int stepsToFeatureWall = graphInfo.calcStepsToNearestCellType(snake.headPosition(),
                    CellType.FEATUREWALL,
                    GraphContainer.GraphType.ONLY_FEATURE_WALL);
            this.displayDebug("amount of steps: " + stepsToFeatureWall);
            return this.scaleLowerStepsHigherImpact(stepsToFeatureWall);
        } else {
            displayDebug("no Feature Wall on Map");
            return 0.0;
        }
    }

/*----------------------------------------------------------------------------*/
//  ChangeHeadTail Feature
    /**
     * checks if there is a ChangeHeadTail Field and calculates the steps to
     * it
     *
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double calcStepsToFeature_ChangeHeadTail(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        if (gameInfo.field().hasChangeHeadTail()) {
            int stepsToFeatureChangeHeadTail = graphInfo.calcStepsToNearestCellType(snake.headPosition(),
                    CellType.CHANGEHEADTAIL,
                    GraphContainer.GraphType.ONLY_FEATURE_CHANGEHEADTAIL);
            this.displayDebug("amount of steps: " + stepsToFeatureChangeHeadTail);
            return this.scaleLowerStepsHigherImpact(stepsToFeatureChangeHeadTail);
        }
        displayDebug("no Feature change head tail on Map");
        return 0.0;
    }

/*----------------------------------------------------------------------------*/
//  ChangeSnake Feature
    /**
     * checks if there is a ChangeSnake Field and calculates the distance to it
     * if the enemy snake is shorter
     *
     * @param graphInfo
     * @param snake
     * @param gameInfo
     * @return
     */
    public double calcStepsToFeature_ChangeSnake(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        // if enemy.size < friendly.size
        if (getOtherSnake(snake, gameInfo).segments().size() < snake.segments().size()
                && gameInfo.field().hasFeatureWall()) {
            int stepsToFeatureChangeSnake = graphInfo.calcStepsToNearestCellType(snake.headPosition(),
                    CellType.CHANGESNAKE,
                    GraphContainer.GraphType.ONLY_FEATURE_CHANGESNAKE);
            this.displayDebug("amount of Steps: " + stepsToFeatureChangeSnake);
            return this.scaleLowerStepsHigherImpact(stepsToFeatureChangeSnake);
        } else {
            displayDebug("no Feature change snake  on Map");
            return 0.0;
        }
    }

    
 /*----------------------------------------------------------------------------*/
//  SpeedUP Feature


/*----------------------------------------------------------------------------*/
//  ChangeSnake Feature
    /**
     * checks if there is a SpeedUP Field and calculates the steps to it
     *
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double calcStepsToFeature_SpeedUP(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        // if enemy.size < friendly.size
        if (gameInfo.field().hasSpeedUp()) {
            int stepsToFeatureSpeedUp = graphInfo.calcStepsToNearestCellType(snake.headPosition(),
                    CellType.SPEEDUP,
                    GraphContainer.GraphType.ONLY_FEATURE_SPEEDUP);
            this.displayDebug("amount of Steps: " + stepsToFeatureSpeedUp);
            return this.scaleLowerStepsHigherImpact(stepsToFeatureSpeedUp);
        } else {
            displayDebug("no Feature speedup on Map");
            return 0.0;
        }
    }

/*----------------------------------------------------------------------------*/
//  CutTail Feature
    /**
     * checks if there is a SpeedUP Field and calculates the steps to it
     *
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double calcStepsToFeature_CutTail(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        // if enemy.size < friendly.size
        if (gameInfo.field().hasCutTail()) {
            int stepsToFeatureCutTail = graphInfo.calcStepsToNearestCellType(snake.headPosition(),
                    CellType.CUTTAIL,
                    GraphContainer.GraphType.ONLY_FEATURE_CUTTAIL);
            this.displayDebug("amount of Steps: " + stepsToFeatureCutTail);
            return this.scaleLowerStepsHigherImpact(stepsToFeatureCutTail);
        } else {
            displayDebug("no Feature CutTail on Map");
            return 0.0;
        }
    }

/*----------------------------------------------------------------------------*/
//  OpenField Feature
    /**
     * checks if there is a SpeedUP Field and calculates the steps to it
     *
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
 public double calcStepsToFeature_OpenField(Snake snake, GraphContainer graphInfo, GameInfo gameInfo){
	  // if enemy.size < friendly.size
	 if (gameInfo.field().hasOpenField()){
            int stepsToFeatureOpenField = graphInfo.calcStepsToNearestCellType(
                    snake.headPosition(), 
                    CellType.OPENFIELD, 
                    GraphContainer.GraphType.ONLY_FEATURE_OPENFIELD);
            this.displayDebug("amount of Steps: " + stepsToFeatureOpenField);
            return this.scaleLowerStepsHigherImpact(stepsToFeatureOpenField);
        }
        else{
            displayDebug("no Feature CutTail on Map");
            return 0.0;
        }
    }

    
/*----------------------------------------------------------------------------*/
//  Direction Calculation
/*----------------------------------------------------------------------------*/
    /**
     * is called in nextDirection. Returns the Direction the snake should go
     * PseudoCOde getInputArrayForDirection calcDirection super.getDirection
     *
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public abstract double[] getScoreAllDirections(
            Snake snake,
            GraphContainer graphInfo,
            GameInfo gameInfo);

    /**
     * creates an 2D array where the input values for each directions are stored
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double[][] getEveryInput(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        double[][] direc = new double[4][];
          direc[0] = getInputArrayForDirectionThread(Direction.UP, snake,graphInfo, gameInfo);
          direc[1] = getInputArrayForDirectionThread(Direction.RIGHT, snake,graphInfo, gameInfo);
          direc[2] = getInputArrayForDirectionThread(Direction.DOWN, snake,graphInfo, gameInfo);
          direc[3] = getInputArrayForDirectionThread(Direction.LEFT, snake,graphInfo, gameInfo);
//        direc[0] = getInputArrayForDirection(Direction.UP, snake, graphInfo, gameInfo);
//        direc[1] = getInputArrayForDirection(Direction.RIGHT, snake, graphInfo, gameInfo);
//        direc[2] = getInputArrayForDirection(Direction.DOWN, snake, graphInfo, gameInfo);
//        direc[3] = getInputArrayForDirection(Direction.LEFT, snake, graphInfo, gameInfo);
        return direc;
    }


    /**
     * calculates the inputArray for a given direction using the thread classes
     * or an empty array if the is no valid move
     *
     * @param d
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double[] getInputArrayForDirectionThread(Direction d, Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        Snake simulatedSnake = new SnakeExt(snake.segments());
        if (d != null) {
            graphInfo = SerializationUtils.clone(graphInfo);
            Point newHead = simulateHead(d, simulatedSnake);
            ((SnakeExt) simulatedSnake).simulateMove(newHead);
            graphInfo.simulateSnakeMovement(newHead, snake);
        }

        // creates a thread for every valid direction
        if (isMoveValid(d, snake, gameInfo)) {
            DirectionThread dirThread = new DirectionThread(simulatedSnake, graphInfo, gameInfo, this);
            dirThread.start();
            try {
                dirThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return dirThread.getDirectionValues();
        }

        double[] scoreArr = new double[amountOfFunctionsUsedForInput];
        for (int i = 0; i < scoreArr.length; i++) {
            scoreArr[i] = 0;
        }
        return scoreArr;
    }

    /**
     * For each Direction the different calculated Values are stored in an
     * "InputArray". Bsp. the normal Brain sums up all input arrays for each
     * direction and compares them. On the other hand the neural network uses
     * all InputArrays concatenated as neural net input.
     *
     * @param d
     * @param snake
     * @param graphInfo
     * @param gameInfo
     * @return
     */
    public double[] getInputArrayForDirection(
            Direction d,
            Snake snake,
            GraphContainer graphInfo,
            GameInfo gameInfo) {

        this.displayDebug("------------------");
        this.displayDebug("Calculating Input Array for Direction: " + d);
        double[] scoreArr = new double[amountOfFunctionsUsedForInput];

        /* simulate a snake into the given direction and update graphs according 
         to movement */
        Snake simulatedSnake = new SnakeExt(snake.segments());
        if (d != null) {
            graphInfo = SerializationUtils.clone(graphInfo);
            Point newHead = simulateHead(d, simulatedSnake);
            ((SnakeExt) simulatedSnake).simulateMove(newHead);
            graphInfo.simulateSnakeMovement(newHead, snake);
        }

        // only if move is valid calculate score, else return 0.0 for each arrayitem
        if (isMoveValid(d, snake, gameInfo)) {
            this.displayDebug("move is valid");

////------- Steps to Apple                   
            this.displayDebug("---");
            this.displayDebug("steps to Apple");
            long startTime = System.nanoTime();
            scoreArr[0] = this.calcStepsToApple(simulatedSnake, graphInfo, gameInfo);
            this.displayDebug("normalized: " + scoreArr[0]);
            this.displayUsedTime(startTime, System.nanoTime());

////------- Distance to Head            
//            this.displayDebug("---");
//            this.displayDebug("DistanceTo Enemy Head");
//            startTime = System.nanoTime();
//            scoreArr[1] = this.distanceToHeadEnemy(simulatedSnake, gameInfo);
//            estimatedTime = System.nanoTime() - startTime;
//            estimatedTime = TimeUnit.NANOSECONDS.toMillis(estimatedTime);
//            this.displayDebug("normalized: " + scoreArr[1]);
//            this.displayDebug("time used: " + estimatedTime);
////------- Distance to own Tail            
//            this.displayDebug("---");
//            this.displayDebug("euc. distance to own tail");
//            startTime = System.nanoTime();
//            scoreArr[2] = this.distanceToOwnTail(simulatedSnake);
//            estimatedTime = System.nanoTime() - startTime;
//            estimatedTime = TimeUnit.NANOSECONDS.toMillis(estimatedTime);
//            this.displayDebug("normalized " + scoreArr[2]);
//            this.displayDebug("time used: " + estimatedTime);

//------- max Steps Enemy snake can make            
            this.displayDebug("---");
            this.displayDebug("max Steps Enemy");
            startTime = System.nanoTime();
            scoreArr[3] = this.maxStepsEnemy(snake, graphInfo, gameInfo);
            this.displayDebug("normalized " + scoreArr[3]);
            this.displayUsedTime(startTime, System.nanoTime());

//------- max Steps Own snake can make            
            this.displayDebug("---");
            this.displayDebug("max Steps own Snake");
            startTime = System.nanoTime();
            scoreArr[4] = this.maxStepsOwnSnake(snake, graphInfo, gameInfo);
            this.displayDebug("normalized " + scoreArr[4]);
            this.displayUsedTime(startTime, System.nanoTime());

//-------------------------STEPS TO FEATURES------------------------------------
//------- max Steps Feature Change Snake
            this.displayDebug("---");
            this.displayDebug("max Steps Feature Change Snake");
            startTime = System.nanoTime();
            scoreArr[5] = this.calcStepsToFeature_ChangeSnake(simulatedSnake, graphInfo, gameInfo);
            this.displayDebug("normalized " + scoreArr[5]);
            this.displayUsedTime(startTime, System.nanoTime());
//            
//------- max Steps Feature Wall            
            this.displayDebug("---");
            this.displayDebug("max Steps to Feature Wall");
            startTime = System.nanoTime();
            scoreArr[6] = this.calcStepsToFeature_Wall(simulatedSnake, graphInfo, gameInfo);
            this.displayDebug("normalized " + scoreArr[6]);
            this.displayUsedTime(startTime, System.nanoTime());


//------- max Steps Feature CutTail           

            this.displayDebug("---");
            this.displayDebug("max Steps to Feature CutTail");
            startTime = System.nanoTime();
            scoreArr[7] = this.calcStepsToFeature_CutTail(simulatedSnake, graphInfo, gameInfo);
            this.displayDebug("normalized " + scoreArr[7]);
            this.displayUsedTime(startTime, System.nanoTime());


//------- max Steps Feature OpenField         

            this.displayDebug("---");
            this.displayDebug("max Steps to Feature OpenField");
            startTime = System.nanoTime();
            scoreArr[8] = this.calcStepsToFeature_OpenField(simulatedSnake, graphInfo, gameInfo);
            this.displayDebug("normalized " + scoreArr[8]);
            this.displayUsedTime(startTime, System.nanoTime());

//------- max Steps Feature Head Tail            
            this.displayDebug("---");
            this.displayDebug("max Steps to Feature Change Head Taill");
            startTime = System.nanoTime();
            scoreArr[9] = this.calcStepsToFeature_ChangeHeadTail(simulatedSnake, graphInfo, gameInfo);
            this.displayDebug("normalized " + scoreArr[9]);
            this.displayUsedTime(startTime, System.nanoTime());

//------- max Steps Feature Portal            
            this.displayDebug("---");
            this.displayDebug("max Steps to feature Portal");
            startTime = System.nanoTime();
            scoreArr[10] = this.calcStepsToFeature_Portal(simulatedSnake, graphInfo, gameInfo);
            this.displayDebug("normalized " + scoreArr[10]);
            this.displayUsedTime(startTime, System.nanoTime());
            this.displayDebug("----------------------------\n");

//------- max Steps Feature SpeedUP            
            this.displayDebug("---");
            this.displayDebug("max Steps to Feature Change SpeedUP");
            startTime = System.nanoTime();
            scoreArr[11] = this.calcStepsToFeature_SpeedUP(snake, graphInfo, gameInfo);
            this.displayDebug("normalized " + scoreArr[11]);
            this.displayUsedTime(startTime, System.nanoTime());
        }
        return scoreArr;
    }

    /**
     * returns the direction the snake will move in the next game tick
     * getMaxScoringDirection has to be overwritten in childclass
     *
     * @param gameInfo
     * @param snake
     * @return
     */
    @Override
    public Direction nextDirection(GameInfo gameInfo, Snake snake) {
        // check against which Snake we are playing
        snakePredictor.detectEnemySnake(snake, gameInfo);

        // generate graph for move
        GraphContainer graphInfo = new GraphContainer(snake.headPosition(), gameInfo.field());

        // has to be overriden by childclasses
        double[] result = getScoreAllDirections(snake, graphInfo, gameInfo);
        this.displayDebug("score Array: \t" + Arrays.toString(result));
        Direction d = getDirection(result[0], result[1], result[2], result[3]);

        // log moves if Wanted
        snakePredictor.logMovesEnemySnake(snake, gameInfo);
        snakePredictor.logMovesAwesomeSnake(snake, gameInfo);

        // set Wall
        setWall(snake, graphInfo, gameInfo);

        // update gameTicks
        gameTicks++;
        return d;
    }

    /**
     * compares each direction and returns the one with the highest score or if
     * the scores are equal, the direction that comes first in the if statements
     *
     * @param scUP
     * @param scRight
     * @param scDown
     * @param scLeft
     * @return Direction the snake moves next
     */
    public Direction getDirection(double scUP, double scRight, double scDown, double scLeft) {
        if (scUP >= scRight
                && scUP >= scLeft
                && scUP >= scDown) {
            return Direction.UP;
        } else if (scDown >= scLeft
                && scDown >= scRight) {
            return Direction.DOWN;
        } else if (scRight >= scLeft) {
            return Direction.RIGHT;
        } else {
            return Direction.LEFT;
        }
    }


    /*----------------------------------------------------------------------------*/
//  Helper Functions
/*----------------------------------------------------------------------------*/
    /**
     * generates a Filename for a chromosom example:
     * "chromosom-AwesomeNormalBrain-Jul_11_00.37.08Uhr-FIT_32.5"
     *
     * @param classname
     * @return
     */
    public String createFilename(String classname) {
        String filename = "chromosom-" + classname;
        filename += String.format("-%tb_%td_%tH.%tM.%tSUhr",
                new Date(), new Date(), new Date(), new Date(), new Date());
        filename += "-FIT_" + round(getFitness(), 1);
        return filename;
    }

    /**
     * scales an the steps made to a value between [0,1] the Lower the steps are
     * the higher the value is near 1 example; the more steps it is to for a
     * direction to a goal (like apple) then the less impact it has
     *
     * @param steps
     * @return [0,1]
     */
    private double scaleLowerStepsHigherImpact(double steps) {
        if (steps == 0) {
            return 1.0;
        } else {
            return 1.0 / steps;
        }
    }

    /**
     * scales an the steps made to a value between [0,1] the higher the steps
     * are the higher the value is near 1 example: the higher steps my snake can
     * make the higher the factor should be
     *
     * @param steps
     * @return [0,1]
     */
    private double scaleHigherStepsHigherImpact(double steps) {
        if (steps == 0) {
            return 0.0;
        } else {
            return 1 - (1.0 / steps);
        }
    }

    /**
     * rounds a given double value to amount of aftercomma digits
     *
     * @param value
     * @param aftercomma
     * @return
     */
    public static double round(double value, int aftercomma) {
        if (aftercomma < 0) {
            throw new IllegalArgumentException();
        }
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(aftercomma, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    /**
     * calculates the Euclidian Distance between two given Points
     *
     * @param point1
     * @param point2
     * @return distance
     */
    private int calcPointDistance(Point point1, Point point2) {
        return Math.abs(point1.x - point2.x) + Math.abs(point1.y - point2.y);
    }

    /**
     * is used to control whether output is silent or not
     * @param s
     */
    private void displayDebug(Object s) {
        if (this.debug) {
            System.out.println(s);
        }
    }

    /**
     * prints time differences is used for measuring timeconsumation of
     * functions
     *
     * @param startTime
     * @param endTime
     */
    private void displayUsedTime(long startTime, long endTime) {
        long estimatedTime = endTime - startTime;
        estimatedTime = TimeUnit.NANOSECONDS.toMillis(estimatedTime);
        this.displayDebug("time used: " + estimatedTime + " ms");
    }

    /**
     * sets the Head to a the Direction given in the Parameter
     *
     * @param d
     * @param snake
     * @return
     */
    private Point simulateHead(Direction d, Snake snake) {
        Point newHead = new Point(snake.headPosition().x, snake.headPosition().y);
        switch (d) {
            case DOWN:
                newHead.y++;
                break;
            case LEFT:
                newHead.x--;
                break;
            case RIGHT:
                newHead.x++;
                break;
            case UP:
                newHead.y--;
                break;
            default:
                break;
        }
        return newHead;
    }
}
