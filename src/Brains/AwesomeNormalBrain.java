package Brains;
import Logic.GameInfo;
import Logic.Snake;
import Util.awesome.graph.GraphContainer;
import Util.awesome.neuralNet.DataReader;


/**
 * Awesome Normal Brain calculates the score for each direction by multiplying
 * each input value for each direction with a wheight. Then it calculates the 
 * sum for each direction. 
 */
public class AwesomeNormalBrain extends AwesomeBrain{
    
    /*
        the following Strings are the specific neural nets for each Enemy and
        a generic one for all. The wheights are encoded in the files
    */
    private final String filename_Generic=
//            "normal-testBrain.data";
            "normal_HandmadeBrain.data";
    private final String filename_HorstAI=
            "chromosom-AwesomeNormalBrain-Jul_11_19.55.30Uhr-WR_0.5-FIT_12.3-RandomBrain.data";
    private final String filename_NCageBrain=
            "chromosom-AwesomeNormalBrain-Jul_11_19.55.30Uhr-WR_0.5-FIT_12.3-RandomBrain.data";
    private final String filename_RandomBrian=
            "chromosom-AwesomeNormalBrain-Jul_11_19.55.30Uhr-WR_0.5-FIT_12.3-RandomBrain.data";
    private final String filename_NotSoRandomBrain1=
            "chromosom-AwesomeNormalBrain-Jul_11_19.55.30Uhr-WR_0.5-FIT_12.3-RandomBrain.data";
    private final String filename_SuperBrain=
            "chromosom-AwesomeNormalBrain-Jul_11_19.55.30Uhr-WR_0.5-FIT_12.3-RandomBrain.data";
    private final String filename_WallBrain=
            "chromosom-AwesomeNormalBrain-Jul_11_19.55.30Uhr-WR_0.5-FIT_12.3-RandomBrain.data";

/*----------------------------------------------------------------------------*/
//  CONSTRUCTORS
/*----------------------------------------------------------------------------*/         
    public AwesomeNormalBrain(){
        super();
    }
    
    public AwesomeNormalBrain(double [] chromosom){
        super(chromosom);                
    }
    
/*----------------------------------------------------------------------------*/
//  Genetic Algorithm
/*----------------------------------------------------------------------------*/
    /**
     * selects the wheights that should be loaded for an opponent based 
     * on what snakePredictor predicts. It loads the wheights into an array
     * that is the chromosom, which it returns
     * @return 
     */
    @Override
    public double [] loadChromosom() {
        double [][] tempChrom;
        switch(snakePredictor.getEnemySnake()){
            case GENERIC:{
                tempChrom = DataReader.readDoubleMatrix(filename_Generic);
                break;
            }
            case HORSTAI:{
                tempChrom = DataReader.readDoubleMatrix(filename_HorstAI);
                break;
            }
            case  NCageBrain:{
                tempChrom = DataReader.readDoubleMatrix(filename_NCageBrain);
                break;        
            }
            case NotSoRandomBrain1:{
                tempChrom = DataReader.readDoubleMatrix(filename_NotSoRandomBrain1);
                break;
            }
            case RandomBrain:{
                tempChrom = DataReader.readDoubleMatrix(filename_RandomBrian);
                break;
            }
            case SuperBrain:{
                tempChrom = DataReader.readDoubleMatrix(filename_SuperBrain);
                break;
            }   
            case WallBrain:{
                tempChrom = DataReader.readDoubleMatrix(filename_WallBrain);
                break;
            }                    
            default:{
                tempChrom = DataReader.readDoubleMatrix(filename_Generic);
            }
        }        
        return tempChrom[0];
    }
        
    /**
     * writes the Array containing the wheights to a file
     */
    @Override
    public void saveChromosom(){
        String filename = createFilename(this.getClass().toString());
        filename += ".data";
        snakePredictor.writeArrayToFile(chromosom, filename);
    }
    
    /**
     * analog to above but with a postfix
     * @param postfix 
     */
    @Override
    public void saveChromosom(String postfix){
        String filename = createFilename(this.getClass().toString());
        filename = filename.replace("class Brains.", "");
        filename += "-" + postfix;
        filename += ".data";
        snakePredictor.writeArrayToFile(chromosom, filename);
    }
    
    /**
     *  prints each value of the chromosom with the corresponding function
     * the wheight is multiplied with
    */
    @Override
    public void printChromosom() {
        System.out.println("The Chromosom is the following: ");
        System.out.println("steps to apple \t\t" + getChromosom()[0]);
        System.out.println("MaxSeps Enemy Snake \t\t" + getChromosom()[3]);
        System.out.println("MaxSteps Own Snake \t\t" + getChromosom()[4]);        
        System.out.println("steps feature Change Snake\t" + getChromosom()[5]);
        System.out.println("steps feature Wall \t\t" + getChromosom()[6]);
        System.out.println("steps feature CutTail \t\t"+ getChromosom()[7]);
        System.out.println("steps feature OpenField\t\t"+ getChromosom()[8]);
        System.out.println("steps feature ChangeHeadTail\t "+ getChromosom()[9]);
        System.out.println("steps feature Portal \t\t "+ getChromosom()[10]);
        System.out.println("steps feature SpeedUP \t\t "+ getChromosom()[11]);
        System.out.println(" ");
    }

    /**
     * a uniform crossover is used because each gene expresses a phaene:
     * for example a tendency to move towards a feature
     * @param a
     * @param b
     * @return 
     */
    @Override
    public AwesomeBrain crossover(AwesomeBrain a, AwesomeBrain b) {
        double[] childchromosom = uniformCrossover(
                a.getChromosom(),
                b.getChromosom(),
                0.4);
        return new AwesomeNormalBrain(childchromosom);
    }
     
    /**
     * Bitflipmutation is used
     */
    @Override
    public void mutation(){
        super.bitFlipMutation();
    }


    
      
  

/*----------------------------------------------------------------------------*/
//  Direction Explicit Implementation
/*----------------------------------------------------------------------------*/
    
    @Override
    public double [] getScoreAllDirections(Snake snake, GraphContainer graphInfo, GameInfo gameInfo) {
        // get for each direction stuff
        double [][] direc = getEveryInput(snake, graphInfo, gameInfo);
        
        // multiply with factors 
        direc = multiplyWithFactors(direc);
        
        // sum over it
        return sumOverInput(direc);
    }
    

    
    /**
     * calculates the Score for Direction X
     * every calc functions is between [-1,1]
     * the factors are calculated by 
     * @param direc 
     * @return 
     */
    protected double [] sumOverInput(double [][] direc) {
        double [] scoreArr = new double [direc.length];
        // sum over the array
        for(int i =0; i < direc.length; i++){
            double rowScore = 0.0;
            for(int j=0; j < direc[0].length; j++){
                rowScore += direc[i][j];
            }
            scoreArr[i] = rowScore;
        }                
        return scoreArr;
    }

    
    /**
     * multiplies each element of a row with the corresponding factor and returns 
     * the array
     * @param direc
     * @return 
     */
    private double [][] multiplyWithFactors(double [][] direc){
        for (double[] direc1 : direc) {
            for (int j = 0; j < direc[0].length; j++) {
                direc1[j] *= this.chromosom[j];
            }
        }
        return direc;
    }
}
